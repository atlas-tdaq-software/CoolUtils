//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// This class
#include "CoolUtils/CoolWriterEngine.h"
#include "CoolUtils/CoolWritable.h"

#include <CoolApplication/DatabaseSvcFactory.h>
#include <CoolKernel/ChannelId.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/FolderSpecification.h>
#include <CoolKernel/FolderVersioning.h>
#include <CoolKernel/HvsTagLock.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/Record.h>
#include <CoolKernel/RecordSpecification.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/pointers.h>
#include <CoralBase/AttributeList.h>

#include "ers/ers.h"
#include "ers/Issue.h"
#include "ers/LocalContext.h"

#include <memory>
#include <ostream>
#include <string>


// Helper Method

static std::string generateFolderDesc( bool timestamp, bool multichannel, std::string foldername )
{
  // The COOL folder description is comprised of ATLAS-specific xml tags that are used in Athena

  //const std::string runLumiDesc   = "<timeStamp>run-lumi</timeStamp>";
  //const std::string timeStampDesc = "<timeStamp>time</timeStamp>";

  // Is the folder timestamp based or run number/lumi block number based (typical)?
  std::string folderDesc = timestamp ? "<timeStamp>time</timeStamp>" : "<timeStamp>run-lumi</timeStamp>";

  // Single or multi channel folder?
  const std::string singleChannelDesc =
    "<addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>";
  const std::string multiChannelDesc  =
    "<addrHeader><address_header service_type=\"71\" clid=\"1238547719\" /></addrHeader><typeName>CondAttrListCollection</typeName>";

  folderDesc += multichannel ? multiChannelDesc : singleChannelDesc;

  // If there is an online-offline split, we add the offline-version as a key
  const std::string infix =  "/Onl/";
  // FIXME: The CSC uses /ONL/; not sure if that needs to be changed or added here.
  std::string::size_type i = foldername.find(infix);

  if ( i != std::string::npos )
    {
      std::string key = foldername;
      key.erase( i, infix.length() );
      folderDesc += "<key>" + key + "</key>";
    }

  return folderDesc;
}

//
// Retrieve this information with (for example):
//
// $ echo "listinfo /Indet/Onl/Beampos" | AtlCoolConsole.py COOLONL_INDET/COMP200
// ['ATLF', 'ATLAS_COOLPROD', 'atlas_dd']
// __Warning: use COOL_DISABLE_CORALCONNECTIONPOOLCLEANUP__
// Connected to 'oracle://ATLAS_COOLPROD;schema=ATLAS_COOLONL_INDET;dbname=COMP200'
// Welcome to AtlCoolConsole. Type 'help' for instructions.
// >>> Specification for multi-version folder /Indet/Onl/Beampos
//  status (Int32)
//  posX (Float)
//  posY (Float)
//  posZ (Float)
//  sigmaX (Float)
//  sigmaY (Float)
//  sigmaZ (Float)
//  tiltX (Float)
//  tiltY (Float)
//  sigmaXY (Float)
//  posXErr (Float)
//  posYErr (Float)
//  posZErr (Float)
//  sigmaXErr (Float)
//  sigmaYErr (Float)
//  sigmaZErr (Float)
//  tiltXErr (Float)
//  tiltYErr (Float)
//  sigmaXYErr (Float)
// Description: <timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="40774348" /></addrHeader><typeName>AthenaAttributeList</typeName><key>/Indet/Beampos</key>
//



// Custom ERS exceptions

namespace daq
{
  ERS_DECLARE_ISSUE( coolutils,
                     BadCoolWrite,
                     "Writing to COOL failed because: " << explanation,
                     ( (const char*) explanation )
                     )
}


// Constructors

daq::coolutils::CoolWriterEngine::CoolWriterEngine( const std::string& connectionName,
                                                    const std::string& folderName,
                                                    const std::string& tagName,
                                                    bool timestamp,
                                                    bool multichannel )
  : m_connectionName( connectionName )
  , m_folderName( folderName )
  , m_tagName( tagName )
  , m_timestamp( timestamp )
  , m_multichannel( multichannel )
  , m_newFolder( false )
{
  ERS_DEBUG( 0, "Instantiated a CoolWriterEngine for"
             << " connection \"" << m_connectionName << "\""
             << ", folder \"" << m_folderName << "\""
             << ", and tag \"" << m_tagName << "\"" );

  ERS_DEBUG( 0, "Writer is " << ( timestamp ? "time" : "run-lumi" ) << " based"
             << " and " << ( multichannel ? "multi" : "single" ) << " channel" );
}

// Destructor

daq::coolutils::CoolWriterEngine::~CoolWriterEngine()
{
}


// Methods
bool
daq::coolutils::CoolWriterEngine::write( cool::ValidityKey since,
                                         const CoolWritable& object,
                                         unsigned int channel,
                                         bool overwrite ) const
{
  ERS_INFO( "Writing to COOL"
            << " database \"" << m_connectionName << "\""
            << " the folder \"" << m_folderName << "\""
            << " with tag \"" << m_tagName << "\"" );

  ERS_INFO( "Start of IOV is at validity key: " << since );
  ERS_INFO( "Using channel: " << channel );

  try
    {
      cool::RecordSpecification recordSpec;
      object.setRecordSpec( recordSpec );  // Derived class method

      // Open or create the database
      cool::IDatabasePtr database;

      try
        {
          ERS_INFO( "Trying to open database \"" << m_connectionName << "\"" );
          database = cool::DatabaseSvcFactory::databaseService().openDatabase( m_connectionName, false );
          // false means !read_only
        }
      catch ( cool::DatabaseDoesNotExist& e )
        {
          ERS_INFO( "Database \"" << m_connectionName << "\" does not exist. Trying to create it" );
          database = cool::DatabaseSvcFactory::databaseService().createDatabase( m_connectionName );
        }

      if ( ! database )
        {
          daq::coolutils::BadCoolWrite i( ERS_HERE, "Failed to open database" );
          ers::error(i);
          return false;
        }

      ERS_DEBUG( 0, "Successfully opened database" );

      // Fetch or create the folder
      ERS_INFO( "Looking for folder \"" << m_folderName << "\"" );

      if ( ! database->existsFolder( m_folderName ) )
        {
          ERS_INFO( "Folder \"" << m_folderName << "\" does not exist. Trying to create it" );

          // The folder has to be multi-version for this writer to be useful
          cool::FolderSpecification folderSpec( cool::FolderVersioning::MULTI_VERSION, recordSpec );

          // The folder description (meta-data) is not used by COOL but is used and interpreted by Athena
          // We deduce the appropriate folder description from our constructor arguments
          std::string folderDesc = generateFolderDesc( m_timestamp, m_multichannel, m_folderName );
          const bool createParents = true;
          cool::IFolderPtr folder = database->createFolder( m_folderName, folderSpec, folderDesc, createParents );

          // Keep track of the fact that we are operating on a fresh folder
          m_newFolder = true;
          // We will lock the tag we are going to store the object under, unless it is multi-channel.
          // Automatic folder creations are used for testing or local DBs, not for the production DB.
          ERS_INFO( "Successfully created folder with description: \"" << folder->description() << "\"" );
        }

      // Fetch the folder
      cool::IFolderPtr folder = database->getFolder( m_folderName );

      if ( ! folder )
        {
          daq::coolutils::BadCoolWrite i( ERS_HERE, "Failed to get folder" );
          ers::error(i);
          return false;
        }

      ERS_DEBUG( 0, "Retrieved folder \"" << m_folderName << "\"" );

      // Check if the folder conforms with what we expect
      const bool timestamp = ( folder->description().find("<timeStamp>time</timeStamp>") != std::string::npos );
      if ( timestamp != m_timestamp )
        {
          ERS_INFO( "Folder appears to be " << (timestamp ? "time" : "run-lumi") << " based while this Writer is not" );
          daq::coolutils::BadCoolWrite i( ERS_HERE, "Incomptabile CoolWriter for this folder type" );
          ers::error(i);
          return false;
        }
      // FIXME: We should then also check if the multichannel flag agrees with the folder having CondAttrListCollection
      // type. But it doesn't seem we have verified the consistency of enough existing cases to enforce that. /CSC seems
      // not to follow that (perhaps it's not used in athena).

      const cool::ChannelId channelId = channel;
      const cool::ValidityKey until = cool::ValidityKeyMax;

      ERS_DEBUG( 0, "Using validity keys: since=" << since << ", until=" << until );

      // Check if the folder tag is locked
      bool locked = false;

      try
        {
          locked = folder->tagLockStatus( m_tagName ) == cool::HvsTagLock::LOCKED;

          if ( locked )
            {
              ERS_INFO( "Tag \"" << m_tagName << "\" is locked" );

              // Check that the IOV we are closing (if any) is open-ended
              cool::IObjectPtr iov = folder->findObject( since, channelId, m_tagName );

              if ( iov && iov->until() != cool::ValidityKeyMax )
                {
                  ERS_INFO( "Found an existing IOV overlapping with this one that is not open-ended" );
                  daq::coolutils::BadCoolWrite i( ERS_HERE, "Illegal insert over locked IOV" );
                  ers::error(i);
                  return false;
                }

              // Check that the IOV is different from (older) than the one we are appending
              if ( iov && iov->since() == since )
                {
                  if ( ! overwrite )
                    {
                      ERS_INFO( "Found an existing open-ended IOV identical to this one" );
                      daq::coolutils::BadCoolWrite i( ERS_HERE, "Illegal insert over locked, identical IOV" );
                      ers::error(i);
                      return false;
                    }
                  else
                    {
                      ERS_INFO( "Found an existing open-ended IOV identical to this one, but ovewrite flag is set" );
                    }
                }
            }
          else
            {
              ERS_INFO( "Tag \"" << m_tagName << "\" is not locked" );
            }
        }
      catch ( cool::TagNotFound& e )
        {
          ERS_INFO( "Tag \"" << m_tagName << "\" was not found for this folder. Will try to create it (leaving it locked)" );
          // FIXME: The new tag will be left unlocked. This is what makes most sense when writing more than one channel.
          // Note that creating the tag might still fail (with TagExists) if the tag is already used by another folder.

          // Check another way if that tag doesn't exist
          //   if ( folder->existsUserTag( m_tagName) )
          //     {
          //       ERS_INFO( "Tag \"" << m_tagName << "\" does however exist. It cannot be reused without deleting it first" );
          //     }
        }
      catch ( cool::ObjectNotFound& e )
        {
          ERS_INFO( "Tag \"" << m_tagName << "\" is locked but no object was found at start of IOV" );

          if ( ! m_newFolder )
            {
              // This doesn't look safe. I believe it is permissible by the offline UPD1 policy but it doesn't seem like
              // proceeding is what is intended here. One situation in which this can arise is if there exists an object
              // for one channel but not for the given one. We prefer to require that all valid channels be initialized
              // before the tag is locked.
              daq::coolutils::BadCoolWrite i( ERS_HERE, "Illegal insert over unitialized IOV" );
              ers::error(i);
              return false;
            }
          else
            {
              ERS_INFO( "Assume this is ok as we are writing to a new folder" );
            }
        }

      // Now unlock the tag if necessary
      if ( locked )
        {
          ERS_INFO( "Temporarily unlocking tag \"" << m_tagName << "\" in order to append IOV" );
          folder->setTagLockStatus( m_tagName, cool::HvsTagLock::UNLOCKED );
        }

      // Store the object
      cool::Record record( recordSpec );
      object.fillRecord( record );   // Method implemented in the derived class

      try
        {
          const bool userTagOnly = true;
          folder->storeObject( since, until, record, channelId, m_tagName, userTagOnly );
        }
      catch ( cool::TagExists& e )
        {
          // A tag with the same name is already used in some other folder. We can get here if the folder didn't have
          // the requested tag but the tag already existed elsewhere in COOL. (Tag names have to be unique.) We catch
          // this to make the error less confusing in case we just attempted to create the tag because we said we didn't
          // find it (in the folder).
          ERS_INFO( "Tag \"" << m_tagName << "\" is already in use in another folder" );
          daq::coolutils::BadCoolWrite i( ERS_HERE, "Attempt to reuse an existing tag" );
          ers::error(i);
          return false;
          // FIXME: It looks like the object may still have been stored without the tag, which is not what we asked
          // for. This needs to be checked.
        }

      ERS_INFO( "Stored object: " << record.attributeList() );

      // Relock if necessary
      if ( locked || m_newFolder )
        {
          ERS_INFO( ( locked ? "Relock" : "Lock" ) << "ing tag \"" << m_tagName << "\"" );
          folder->setTagLockStatus( m_tagName, cool::HvsTagLock::LOCKED );
        }

      database->closeDatabase();
    }
  catch ( std::exception& e )
    {
      // COOL, CORAL and POOL exceptions inherit from std exceptions,
      // so catching std::exception will catch all errors from COOL,
      // CORAL and POOL.
      daq::coolutils::BadCoolWrite i( ERS_HERE, e.what() );
      ers::error(i);
      // FIXME: close database?
      return false;
    }

  return true;
}


