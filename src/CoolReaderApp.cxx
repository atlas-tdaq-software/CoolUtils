//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/CoolReader.h"
#include "CoolUtils/CoolReadable.h"

#include <boost/lexical_cast/bad_lexical_cast.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/type_index/type_index_facade.hpp>

#include "ers/ers.h"
#include "ers/Issue.h"                                    // for ERS_IS_EMPT...

#include <string>
using std::string;

namespace po = boost::program_options;


// DB connection parameters

std::string defaultDbConnection = "sqlite://;schema=CoolUtils_test.db;dbname=CONDBR2";
std::string defaultDbFolder     = "/CoolUtils/TestParameters";
std::string defaultDbTag        = "CoolUtils-HLT-UPD1-000-00";


int main( int argc, char* argv[] )
{
  enum { SUCCESS=0, BAD_USAGE=1, BAD_RETURN=2, BAD_EXCEPTION=3 };

  // Parse the command line
  po::options_description desc( "Options" );
  desc.add_options()
    ( "help,h", "this help message" )
    ( "connection,c" , po::value<string>()->default_value( defaultDbConnection ), "COOL database connection string" )
    ( "folder,f"     , po::value<string>()->default_value( defaultDbFolder     ), "COOL folder " )
    ( "tag,t"        , po::value<string>()->default_value( defaultDbTag        ), "COOL tag" )

    ( "run,r"        , po::value<unsigned>()->default_value( 0 ), "run number at start of IOV" )
    ( "lb,l"         , po::value<unsigned>()->default_value( 0 ), "lumi block number at start of IOV" )
    ( "channel,i"    , po::value<unsigned>()->default_value( 0 ), "COOL channel ID" )
    ;

  bool success = false;
  po::variables_map vm;
  po::store( po::parse_command_line( argc, argv, desc ), vm );
  po::notify( vm );

  if ( vm.count( "help" ) )
    {
      ERS_INFO( desc );
      return BAD_USAGE;
    }

  try
    {
      daq::coolutils::CoolReadable myObject;
      daq::coolutils::CoolReader reader( vm [ "connection" ].as<string>(),
                                         vm [ "folder"     ].as<string>(),
                                         vm [ "tag"        ].as<string>()
                                         );

      success = reader.read( myObject,
                             vm[ "run"     ].as<unsigned>(),
                             vm[ "lb"      ].as<unsigned>(),
                             vm[ "channel" ].as<unsigned>() );
    }
  catch ( std::exception& e )
    {
      return BAD_EXCEPTION;
    }

  if ( ! success )
    return BAD_RETURN;

  return SUCCESS;
}
