//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// This class
#include "CoolUtils/TimeBasedCoolWriter.h"
#include "CoolUtils/CoolWriterEngine.h"
#include "CoolKernel/ValidityKey.h"

#include "ers/ers.h"
#include "ers/Issue.h"

#include <ctime>
#include <iomanip>
#include <ostream>
#include <string>

// Forward declarations
namespace daq { namespace coolutils { class CoolWritable; } }


// Constructors

daq::coolutils::TimeBasedCoolWriter::TimeBasedCoolWriter( const std::string& connectionName,
                                                          const std::string& folderName,
                                                          const std::string& tagName )
  : m_writer( connectionName, folderName, tagName, true )
{
  ERS_INFO( "Instantiated a TimeBasedCoolWriter for"
            << " connection \"" << connectionName << "\""
            << ", folder \"" << folderName << "\""
            << ", and tag \"" << tagName << "\"" );
}

// Destructor

daq::coolutils::TimeBasedCoolWriter::~TimeBasedCoolWriter()
{
}

// Methods

bool
daq::coolutils::TimeBasedCoolWriter::write( time_t timestamp,
                                            const CoolWritable& object,
                                            unsigned int channel ) const
{
  ERS_INFO( "Start of IOV is timestamp: " << timestamp
            << ", which is: " << std::put_time( localtime(&timestamp), "%c" ) );
  // Note that localtime() uses a static buffer and is not thread-safe (and without a C++ alternative)

  // The validity key is 63 bits with ns precision, timestamp is in seconds since the epoch
  const cool::ValidityKey since = cool::ValidityKey(timestamp) * cool::ValidityKey(1000000000);

  ERS_DEBUG( 0, "Computed start of validity key from timestamp as: since=" << since );
  return m_writer.write( since, object, channel );
}


bool
daq::coolutils::TimeBasedCoolWriter::write( timespec ts,
                                            const CoolWritable& object,
                                            unsigned int channel ) const
{
  ERS_INFO( "Start of IOV is timestamp: " << ts.tv_sec << "." << ts.tv_nsec
            << ", which is: " << std::put_time( localtime(&ts.tv_sec), "%c" )
            << " and " << ts.tv_nsec << " ns" );
  // localtime() is not thread-safe

  // The validity key is 63 bits with ns precision
  const cool::ValidityKey since = cool::ValidityKey(ts.tv_sec) * cool::ValidityKey(1000000000)
    + cool::ValidityKey(ts.tv_nsec);

  ERS_DEBUG( 0, "Computed start of validity key from timestamp as: since=" << since );
  return m_writer.write( since, object, channel );
}
