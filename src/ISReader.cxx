//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/ISReader.h"
#include "CoolUtils/ISReadable.h"

#include "ers/Issue.h"
#include "ers/LocalContext.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/exceptions.h"
#include "ipc/partition.h"
#include "is/info.h"
#include "is/namedinfo.h"

#include <cstdlib>
#include <string>
#include <sstream>
#include <memory>


// Truncate the printout of the IS object in case it is very large
const size_t maxString = 2048;

// Custom ERS declarations

namespace daq 
{
  ERS_DECLARE_ISSUE(	coolutils,
			BadISRead,
			"Reading from IS failed because: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	coolutils,
			InvalidPartition,
			"Partition is not valid: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	coolutils,
			EnvironmentMissing,
			"Environment variable is not defined: " << explanation,
                        ( (const char*) explanation )
                        )
}

// Constructors

daq::coolutils::ISReader::ISReader( const std::string& objectName )
  : m_objectName( objectName )
{
  try
    {
      int argc=1;
      const char* argv[] = { "ISReader" };

      // FIXME: Need to cast away const-ness because IPCCore::init()
      // signature is not const-clean
      IPCCore::init( argc, (char**) argv );
    }
  catch ( daq::ipc::CannotInitialize& e )
    {
      ers::error(e);
      return;
    }
  catch ( daq::ipc::AlreadyInitialized& e )
    {
      // This should be harmless
      ers::log(e);
    }

  // Retrieve TDAQ_PARTITION from the environment
  const char* partitionName = getenv( "TDAQ_PARTITION" );
  if ( partitionName )
    {
      m_partitionName = partitionName;
    }
  else
    {
      daq::coolutils::EnvironmentMissing i( ERS_HERE, "TDAQ_PARTITION" );
      ers::error(i);
      return;
    }
}

// Destructor

daq::coolutils::ISReader::~ISReader()
{
}

// Methods

bool
daq::coolutils::ISReader::read( ISReadable& object ) const
{
  ERS_INFO( "Trying to connect to partition \"" << m_partitionName << "\"" );

  IPCPartition partition( m_partitionName );

  if ( ! partition.isValid() )
    {
      daq::coolutils::InvalidPartition i( ERS_HERE, m_partitionName.c_str() );
      ers::error(i);
      return false;
    }

  ERS_DEBUG( 0, "Successfully connected to partition \"" << partition.name() << "\"" );

  try
    {
      ERS_INFO( "Trying to read IS object \"" << m_objectName << "\"" );

      //      m_objectPtr.reset( object->create( partition, m_objectName ) );
      //      m_objectPtr->checkout();
      object.initialize( partition, m_objectName );
      object.isNamedInfoPtr()->checkout();
      
      std::stringstream ss;
      ss << *object.isNamedInfoPtr();
      std::string objectStr = ss.str().substr(0, maxString);
      if ( ss.str().length() > maxString )
        objectStr += " ...\n(truncated)";
      ERS_INFO( "Read the following object from IS: " << objectStr );
    }
  catch ( ers::Issue& i )
    {
      ers::error(i);
      return false;
    }

  return true;
}
