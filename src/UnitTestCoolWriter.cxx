//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Test suite case for the UnitTestCoolWriter.
//
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoolUtils/CoolReadable.h"
#include "CoolUtils/CoolReadableVector.h"
#include "CoolUtils/CoolReader.h"
#include "CoolUtils/CoolWritable.h"
#include "CoolUtils/CoolWriter.h"
#include "CoolUtils/TimeBasedCoolReader.h"
#include "CoolUtils/TimeBasedCoolWriter.h"

#include <CoolApplication/DatabaseSvcFactory.h>
#include <CoolKernel/FolderSpecification.h>
#include <CoolKernel/FolderVersioning.h>
#include <CoolKernel/HvsTagLock.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IField.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/Record.h>
#include <CoolKernel/RecordSpecification.h>
#include <CoolKernel/StorageType.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/pointers.h>
#include <CoolKernel/types.h>

using namespace daq::coolutils;

#define BOOST_TEST_MODULE UnitTestCoolWriter
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_log.hpp>
#include <boost/test/unit_test_suite.hpp>
#include <boost/test/tools/old/interface.hpp>

#include <cstdio>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <memory>

/**
 * Simple test suite for class CoolWriter.
 * See http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/index.html
 */

namespace {

const std::string folderDescRunLumi = "<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName><key>/Indet/Beampos</key>";
const std::string folderDescTime = "<timeStamp>time</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>";

// implementation of writable interface just for this test
class TestWritable: public CoolWritable {
public:
    TestWritable(int a, float b) : _a(a), _b(b) {}
    virtual ~TestWritable() {}

    // These two methods have to be implemented by the derived class:

    void fillRecord(cool::Record& record) const override {
        record["valueA"].setValue<cool::Int32>(this->_a);
        record["valueB"].setValue<cool::Float>(this->_b);
    }

    void setRecordSpec(cool::RecordSpecification& recordSpec) const override {
        recordSpec.extend("valueA", cool::StorageType::Int32);
        recordSpec.extend("valueB", cool::StorageType::Float);
    }
private:
    int _a;
    float _b;
};


  class TestReadableVector : public CoolReadableVector {
  private:
    unsigned int someData;   // make this class bigger than the base
  };
    
}

// ==============================================================

// Fixture that creates new empty sqlite database
class MakeDb {
public:
    MakeDb(bool lockTag=true, bool runLumi=true, bool createFolder=true ) {
        // make temporary file name
        file = tmpnam(nullptr);
        conn = "sqlite://;schema=" + file + ";dbname=CONDBR2";
        BOOST_TEST_MESSAGE("using file " + file);
        folder = "/FOLDER";
        tag = "TAG-UPD1";

        auto db = cool::DatabaseSvcFactory::databaseService().createDatabase(conn);

        if ( createFolder ) {
            TestWritable tw(0, 0.);
            cool::RecordSpecification recordSpec;
            tw.setRecordSpec(recordSpec);
            cool::FolderSpecification folderSpec(cool::FolderVersioning::MULTI_VERSION, recordSpec);
            auto folderDesc = runLumi ? folderDescRunLumi : folderDescTime;
            auto fldr = db->createFolder(folder, folderSpec, folderDesc, true);
            BOOST_TEST_MESSAGE("created database and folder");

            cool::Record record(recordSpec);
            tw.fillRecord(record);
            fldr->storeObject(cool::ValidityKeyMin, cool::ValidityKeyMax, record, 0, tag, true);
            BOOST_TEST_MESSAGE("stored default record");
            if (lockTag) {
              fldr->setTagLockStatus(tag, cool::HvsTagLock::LOCKED);
              BOOST_TEST_MESSAGE("locked tag");
            }
        }
    }

    ~MakeDb() {
        //        unlink(file.c_str());
        remove(file.c_str());
        BOOST_TEST_MESSAGE("deleted file " + file);
    }

    std::string file;
    std::string conn;
    std::string folder;
    std::string tag;
};

// ==============================================================

BOOST_AUTO_TEST_CASE(test_no_lock)
{
    MakeDb fixture(false);

    CoolWriter writer(fixture.conn, fixture.folder, fixture.tag);

    // we should be able to write any interval overlaps
    TestWritable tw(1, 2.);

    BOOST_CHECK(writer.write(100, 0, tw));
    BOOST_CHECK(writer.write(100, 10, tw));
    BOOST_CHECK(writer.write(101, 10, tw));
    BOOST_CHECK(writer.write(100001, 100000, tw));
    BOOST_CHECK(writer.write(100001, 100000, tw));
    BOOST_CHECK(writer.write(100001, 0, tw));
    BOOST_CHECK(writer.write(1, 0, tw));
    BOOST_CHECK(writer.write(0, 0, tw));
}

// ==============================================================

BOOST_AUTO_TEST_CASE(test_lock)
{
    MakeDb fixture;

    CoolWriter writer(fixture.conn, fixture.folder, fixture.tag);

    // only moving forward is allowed
    TestWritable tw(1, 2.);

    BOOST_CHECK(writer.write(100, 0, tw));            // ok- first run
    BOOST_CHECK(writer.write(100, 10, tw));           // ok- later lb
    BOOST_CHECK(writer.write(101, 10, tw));           // ok- next run
    BOOST_CHECK(writer.write(100001, 100000, tw));    // ok- later run and lb
    BOOST_CHECK(!writer.write(100001, 100000, tw));   // not ok- same run and lb
    BOOST_CHECK(!writer.write(100001, 0, tw));        // not ok- same run but earlier lb
    BOOST_CHECK(!writer.write(1, 0, tw));             // not ok- even earlier run
    BOOST_CHECK(!writer.write(0, 0, tw));             // not ok- same
    BOOST_CHECK(writer.write(100002, 0, tw));         // ok again- new run
}

// ==============================================================

BOOST_AUTO_TEST_CASE(test_lock_owerwrite)
{
    MakeDb fixture;

    CoolWriter writer(fixture.conn, fixture.folder, fixture.tag);

    // moving forward or overwriting of the same interval is allowed
    TestWritable tw(1, 2.);

    BOOST_CHECK(writer.overwrite(100, 0, tw));
    BOOST_CHECK(writer.overwrite(100, 10, tw));
    BOOST_CHECK(writer.overwrite(101, 10, tw));
    BOOST_CHECK(writer.overwrite(100001, 100000, tw));
    BOOST_CHECK(writer.overwrite(100001, 100000, tw));
    BOOST_CHECK(!writer.overwrite(100001, 0, tw));
    BOOST_CHECK(!writer.overwrite(1, 0, tw));
    BOOST_CHECK(!writer.overwrite(0, 0, tw));
    BOOST_CHECK(writer.overwrite(100002, 0, tw));
}

// ==============================================================

#include <ctime>

BOOST_AUTO_TEST_CASE(test_timebased_wrong_type)
{
    MakeDb fixture; // run-lumi folder is the default

    TimeBasedCoolWriter writer(fixture.conn, fixture.folder, fixture.tag);
    TestWritable tw(1, 2.);

    BOOST_CHECK(!writer.write(0, tw));    // not ok- wrong folder type
}

// ==============================================================

BOOST_AUTO_TEST_CASE(test_timebased_lock)
{
    MakeDb fixture(true, false);

    TimeBasedCoolWriter writer(fixture.conn, fixture.folder, fixture.tag);

    struct std::tm tm;
    std::istringstream ss("2020-03-01 15:30:45");
    ss >> std::get_time(&tm, "%Y-%m-%d %H:%M:%S");
    std::time_t since = mktime(&tm);

    BOOST_TEST_MESSAGE("time_t is: " + std::string(ctime(&since)));

    // only moving forward is allowed
    TestWritable tw(1, 2.);

    BOOST_CHECK( writer.write(since   ,    tw));    // ok- first entry
    BOOST_CHECK( writer.write(since+60,    tw));    // ok- a minute later
    BOOST_CHECK( writer.write(since+86400, tw));    // ok- a day later
    BOOST_CHECK(!writer.write(since+86400, tw));    // not ok- same time as before
    BOOST_CHECK(!writer.write(since+60,    tw));    // not ok- earlier time than before
    BOOST_CHECK(!writer.write(since+120,   tw));    // not ok- later but still too early
    BOOST_CHECK(!writer.write(0,           tw));    // not ok- never again
    BOOST_CHECK( writer.write(since+86401, tw));    // ok again- new day
}

// ==============================================================

BOOST_AUTO_TEST_CASE(test_timebased_creating_folder)
{
    MakeDb fixture(true, false, false);

    TimeBasedCoolWriter writer(fixture.conn, fixture.folder, fixture.tag);

    TestWritable tw(1, 2.);

    const time_t t = 1500000000;
    BOOST_CHECK( writer.write(t,       tw));    // ok- folder will be created and locked
    BOOST_CHECK( writer.write(t+60,    tw));    // ok- folder will be used, found to be locked
    BOOST_CHECK(!writer.write(t+30,    tw));    // not ok- new tag is now locked
}


// ==============================================================


BOOST_AUTO_TEST_CASE(test_timebased_reading)
{
    MakeDb fixture(true, false, false);        // time-based and folder not created

    TimeBasedCoolWriter writer(fixture.conn, fixture.folder, fixture.tag);

    TestWritable tw1(1, 2.);
    TestWritable tw2(2, 2.);
    TestWritable tw3(3, 2.);
    const time_t t = 1500000000;

    BOOST_CHECK( writer.write(t,     tw1));    // ok- folder will be created and locked
    BOOST_CHECK( writer.write(t+60,  tw2));    // ok- folder will be used, found to be locked
    BOOST_CHECK( writer.write(t+120, tw3));    // ok- folder will be used, found to be locked

    CoolReadable tr1, tr2;

    TimeBasedCoolReader timereader(fixture.conn, fixture.folder, fixture.tag);

    BOOST_CHECK( timereader.read(tr1, t+30) );    // ok- should find the first entry
    BOOST_CHECK( timereader.read(tr2, t+60) );    // ok- should find the second entry
    BOOST_CHECK( tr1 == tw1 );
    BOOST_CHECK( tr2 == tw2 );
    BOOST_CHECK( tr2 != tw1 );

    // Now use the wrong reader type but fake a correct validity key
    // (so we would find an entry if we were allowed to look)
    CoolReader reader(fixture.conn, fixture.folder, fixture.tag);

    // 1,500,000,000 * 10e9 / 2^32 ~= 349,245,966
    unsigned fakerun  = 349245966;
    unsigned fakelumi = 0;
    BOOST_CHECK( !reader.read(tr2, fakerun, fakelumi) );       // not ok- wrong type
}


BOOST_AUTO_TEST_CASE(test_timebased_nanoseconds)
{
    MakeDb fixture(true, false, false);        // time-based and folder not created

    TestWritable tw1( 1, 3.);
    TestWritable tw2( 2, 3.);
    TestWritable tw3( 3, 3.);
    CoolReadable tr1, tr2, tr3;

    TimeBasedCoolWriter timewriter(fixture.conn, fixture.folder, fixture.tag);
    TimeBasedCoolReader timereader(fixture.conn, fixture.folder, fixture.tag);

    const time_t t = 1500000000;
    timespec ts1 { t-1, 999999999 };
    timespec ts2 { t, 0 };
    timespec ts3 { t, 1 };

    {
      //    TimeBasedCoolReader timereader(fixture.conn, fixture.folder, fixture.tag);
    BOOST_CHECK( timewriter.write(ts1, tw1));
    BOOST_CHECK( timereader.read(tr1, ts1) );
    BOOST_CHECK( timereader.read(tr2, ts2) );
    BOOST_CHECK( timereader.read(tr3, ts3) );
    BOOST_CHECK( tr1 == tw1 );     // ok- should find tw1 at same key
    BOOST_CHECK( tr2 == tw1 );     // ok- should read tw1 from open-ended IOV
    BOOST_CHECK( tr3 == tw1 );     // ok- should read tw1 from open-ended IOV
    }
    
    {
      //    TimeBasedCoolReader timereader(fixture.conn, fixture.folder, fixture.tag);
    BOOST_CHECK( timewriter.write(ts2, tw2));
    BOOST_CHECK( timereader.read(tr1, ts1) );
    BOOST_CHECK( timereader.read(tr2, ts2) );
    BOOST_CHECK( timereader.read(tr3, ts3) );
    BOOST_CHECK( tr1 == tw1 );     // ok- should be unchanged
    BOOST_CHECK( tr2 == tw2 );     // ok- should now find tr2 at same key
    BOOST_CHECK( tr2 != tw1 );     // ok- different value
    BOOST_CHECK( tr3 == tw2 );     // ok- should now find tr2 in open-ended IOV
    }

    {    
      //    TimeBasedCoolReader timereader(fixture.conn, fixture.folder, fixture.tag);
    BOOST_CHECK( timewriter.write(ts3, tw3));
    BOOST_CHECK( timereader.read(tr1, ts1) );
    BOOST_CHECK( timereader.read(tr2, ts2) );
    BOOST_CHECK( timereader.read(tr3, ts3) );
    BOOST_CHECK( tr1 == tw1 );     // ok- should be unchanged
    BOOST_CHECK( tr2 == tw2 );     // ok- should be unchanged
    BOOST_CHECK( tr3 == tw3 );     // ok- should now find tr3 at same key
    BOOST_CHECK( tr3 != tw2 );     // ok- different value
    }
    
}


BOOST_AUTO_TEST_CASE(test_timebased_afp)
{
    MakeDb fixture(true, false, false);        // time-based and folder not created

    TestWritable tw1(1, 2.);
    TestWritable tw2(3, 4.);

    // Matthias' test case
    TimeBasedCoolWriter writer1(fixture.conn, "/AFP/ONL/CONFIG/A/NEAR/HITBUS",         "PHYSICS");
    BOOST_CHECK(  writer1.write( 1616619718, tw1 ) );

    TimeBasedCoolWriter writer2(fixture.conn, "/AFP/ONL/CONFIG/A/NEAR/MOD/AFP_B3_M12", "PHYSICS");
    BOOST_CHECK( !writer2.write( 1616619718, tw2 ) );

    TimeBasedCoolWriter writer3(fixture.conn, "/AFP/ONL/CONFIG/A/NEAR/MOD/AFP_B3_M12", "PHYSICS_M12");
    BOOST_CHECK(  writer3.write( 1616619718, tw2 ) );
}


BOOST_AUTO_TEST_CASE(test_timebased_multichannel)
{
    MakeDb fixture(true, false, false);        // time-based and folder not created

    TestWritable tw0(0, 5.);
    TestWritable tw1(1, 5.);
    TestWritable tw2(2, 5.);
    TestWritable tw3(3, 5.);

    // Matthias' test case
    TimeBasedCoolWriter writer1(fixture.conn, "/AFP/ONL/CONFIG/A/NEAR/P", "AfpConfigANearP-UPD1-00");

    const time_t t = 1616619718;
    BOOST_CHECK( writer1.write( t, tw0, 0 ) );     // ok - writing to a new folder
    BOOST_CHECK( writer1.write( t, tw1, 1 ) );     // ok - writing another channel to a folder just created
    BOOST_CHECK( writer1.write( t, tw2, 2 ) );     // ok - also ok

    TimeBasedCoolWriter writer2(fixture.conn, "/AFP/ONL/CONFIG/A/NEAR/P", "AfpConfigANearP-UPD1-00");

    BOOST_CHECK( !writer2.write( t+100, tw3, 3 ) );    // not ok - trying to write a new channel to a locked existing folder
    BOOST_CHECK(  writer2.write( t+100, tw3, 1 ) );    // ok - appending to an existing channel

    // Read back all channels individually
    TimeBasedCoolReader reader(fixture.conn, "/AFP/ONL/CONFIG/A/NEAR/P", "AfpConfigANearP-UPD1-00");
    CoolReadable tr0, tr1, tr2;
    BOOST_CHECK( reader.read( tr0, t, 0 ) );
    BOOST_CHECK( reader.read( tr1, t, 1 ) );
    BOOST_CHECK( reader.read( tr2, t, 2 ) );
    BOOST_CHECK( tr0 == tw0 );
    BOOST_CHECK( tr1 == tw1 );
    BOOST_CHECK( tr2 == tw2 );
    
    // Now try to read back all channels at once and compare
    std::vector<CoolReadable> tvec;
    BOOST_CHECK( reader.read( tvec, t ) );
    BOOST_CHECK( tvec.size() == 3 );
    BOOST_CHECK( tvec[0] == tw0 );
    BOOST_CHECK( tvec[1] == tw1 );
    BOOST_CHECK( tvec[2] == tw2 );

    BOOST_CHECK( reader.read( tvec, t+200 ) );
    BOOST_CHECK( tvec.size() == 3 );
    BOOST_CHECK( tvec[0] == tw0 );
    BOOST_CHECK( tvec[1] == tw3 );
    BOOST_CHECK( tvec[2] == tw2 );

    // Now read into a custom vector
    TestReadableVector trvec;
    BOOST_CHECK( reader.read( trvec, t ) );
    //    auto v = *trvec.objects();
    //    BOOST_CHECK( v.size() == 3 );
    BOOST_CHECK( trvec.objects()->size() == 3 );
    BOOST_CHECK( trvec.object(0) == tw0 );
    BOOST_CHECK( trvec.object(1) == tw1 );
    BOOST_CHECK( trvec.object(2) == tw2 );

    // Trigger a range check
    CoolReadable nothing_read;
    BOOST_CHECK( trvec.object(3) == nothing_read );
}


BOOST_AUTO_TEST_CASE(test_tagexists)
{
    MakeDb fixture(false, false, false);          // run-lumi based and folder not created

    TestWritable w1( 1, 1. );
    TestWritable w2( 2, 2. );

    CoolWriter writer1(fixture.conn, "/Subsystem/Folder1", "SubsystemFolder1-UPD1-00");
    BOOST_CHECK(  writer1.write( 500000, 0, w1 ) );

    CoolWriter writer2(fixture.conn, "/Subsystem/Folder2", "SubsystemFolder1-UPD1-00");
    BOOST_CHECK( !writer2.write( 500000, 0, w2 ) );       // not ok- same tag

    CoolReadable r1, r2;
    BOOST_CHECK( r1 != w1 );   // nothing read yet
    BOOST_CHECK( r2 != w2 );   // nothing read yet

    CoolReader reader1(fixture.conn, "/Subsystem/Folder1", "SubsystemFolder1-UPD1-00");
    BOOST_CHECK(  reader1.read( r1, 500000, 0 ) );
    BOOST_CHECK( r1 == w1 );

    CoolReader reader2(fixture.conn, "/Subsystem/Folder2", "SubsystemFolder1-UPD1-00");
    BOOST_CHECK( !reader2.read( r2, 500000, 0 ) );       // not ok- nothing written
    BOOST_CHECK( r2 != w2 );
}
