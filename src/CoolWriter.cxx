//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// This class
#include "CoolUtils/CoolWriter.h"
#include "CoolUtils/CoolWriterEngine.h"

#include "CoolKernel/ValidityKey.h"

#include "ers/ers.h"
#include "ers/Issue.h"

#include <cstdint>
#include <string>
#include <ostream>


// Forward declarations
namespace daq { namespace coolutils { class CoolWritable; } }


// Constructors

daq::coolutils::CoolWriter::CoolWriter( const std::string& connectionName,
                                        const std::string& folderName,
                                        const std::string& tagName )
  : m_writer( connectionName, folderName, tagName, false )
{
  ERS_INFO( "Instantiated a CoolWriter for"
            << " connection \"" << connectionName << "\""
            << ", folder \"" << folderName << "\""
            << ", and tag \"" << tagName << "\"" );
}

// Destructor

daq::coolutils::CoolWriter::~CoolWriter()
{
}

// Methods

bool
daq::coolutils::CoolWriter::write( unsigned int runNumber,
                                   unsigned int lumiBlockNumber,
                                   const CoolWritable& object,
                                   unsigned int channel ) const
{
  return _write( runNumber, lumiBlockNumber, object, channel, false);
}


bool
daq::coolutils::CoolWriter::overwrite( unsigned int runNumber,
                                       unsigned int lumiBlockNumber,
                                       const CoolWritable& object,
                                       unsigned int channel) const
{
  return _write( runNumber, lumiBlockNumber, object, channel, true );
}


// Private methods

bool
daq::coolutils::CoolWriter::_write( unsigned int runNumber,
                                    unsigned int lumiBlockNumber,
                                    const CoolWritable& object,
                                    unsigned int channel,
                                    bool overwrite ) const
{
  ERS_INFO( "Start of IOV is run number: " << runNumber
            << ", lumi block number: " << lumiBlockNumber );

  // Index IOV by run number and lumi block
  const uint64_t a = runNumber;
  const cool::ValidityKey since = ((a<<32) | lumiBlockNumber);

  ERS_DEBUG( 0, "Computed start of validity key from run and lumi block number as: since=" << since );

  return m_writer.write( since, object, channel, overwrite );

}
