//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/TestParameters.h"
#include "CoolUtils/ISWriter.h"

#include "ers/ers.h"
#include "ers/Issue.h"

#include <boost/lexical_cast/bad_lexical_cast.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/type_index/type_index_facade.hpp>

#include <vector>
#include <string>

using std::vector;
using std::string;

namespace po = boost::program_options;

const std::string defaultServerName = "CoolUtils";
const std::string defaultObjectName = "TestParameters";


int main( int argc, char* argv[] )
{
  enum { SUCCESS=0, BAD_USAGE=1, BAD_COMMAND=2 };

  // Parse the command line
  po::options_description desc( "Options" );
  desc.add_options()
    ( "help", "this help message" )
    ( "server-name", po::value<string>()->default_value( defaultServerName ), "name of the IS server to publish to" )
    ( "name"       , po::value<string>()->default_value( defaultObjectName ), "name under which to publish the IS object" )
    ( "test-parameters" , po::value< vector<string> >(), "list of test parameters" )
    ;

  po::positional_options_description pod;
  pod.add( "test-parameters", -1 );
  po::variables_map vm;
  // We must disable short options in order to be able to parse negative numbers without quoting
  const int s =  po::command_line_style::default_style ^ po::command_line_style::allow_short;
  po::store( po::command_line_parser( argc, argv ).style( s ).options( desc ).positional( pod ).run(), vm );
  po::notify( vm );

  if ( vm.count( "help" ) )
    {
      ERS_INFO( desc );
      return BAD_USAGE;
    }

  const vector<string> positionalPars = vm[ "test-parameters" ].as< vector<string> >();

  daq::TestParameters bsPars;
  bsPars.copyFrom( positionalPars );

  const string isName = vm[ "server-name" ].as<string>() + "." + vm[ "name" ].as<string>();
  daq::coolutils::ISWriter writer( isName );
  const bool success = writer.write( bsPars );
  if ( ! success )
    return BAD_COMMAND;

  return SUCCESS;
}
