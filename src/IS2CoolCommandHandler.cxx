//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/IS2CoolCommandHandler.h"
#include "CoolUtils/IS2CoolTransferrable.h"

#include "ers/ers.h"
#include "ers/Issue.h"
#include "ers/LocalContext.h"

#include <string>
#include <ostream>


// Custom ERS exceptions

namespace daq 
{
  ERS_DECLARE_ISSUE(	coolutils,
			BadIS2Cool,
			"Transfer from IS to COOL failed because " << explanation,
                        ( (const char*) explanation )
                        )
}

// Constructors

daq::coolutils::IS2CoolCommandHandler::IS2CoolCommandHandler( const std::string& dbConnectionName,
                                                              const std::string& dbFolderName,
                                                              const std::string& dbTagName,
                                                              const std::string& isObjectName )
  : m_isReader( isObjectName )
  , m_coolWriter( dbConnectionName, dbFolderName, dbTagName )
{
}

// Destructor

daq::coolutils::IS2CoolCommandHandler::~IS2CoolCommandHandler()
{
}

// Methods

bool
daq::coolutils::IS2CoolCommandHandler::transfer( unsigned int runNumber,
                                                 unsigned int lumiBlockNumber,
                                                 IS2CoolTransferrable& object ) const
{
  bool success = false;

  // Read parameters from IS
  try
    {
      success = m_isReader.read( object );
    }
  catch ( std::exception& e )
    {
      daq::coolutils::BadIS2Cool i( ERS_HERE, e.what() );
      ers::error(i);    
    }

  if ( ! success )
    return false;

  // Write parameters to COOL
  try
    {
      success = m_coolWriter.write( runNumber, lumiBlockNumber, object );
    }
  catch ( std::exception& e )
    {
      daq::coolutils::BadIS2Cool i( ERS_HERE, e.what() );
      ers::error(i);    
    }

  return success;
}
