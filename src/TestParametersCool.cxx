//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/TestParametersCool.h"

#include <CoolKernel/IField.h>
#include <CoolKernel/IRecord.h>
#include <CoolKernel/Record.h>
#include <CoolKernel/types.h>

#include <memory>

using namespace daq;


// Methods

void 
TestParametersCool::copyFrom( const cool::IRecord& record )
{
  m_valueA = record[ "valueA" ].data<cool::Float>();
  m_valueB = record[ "valueB" ].data<cool::Float>();
  m_valueC = record[ "valueC" ].data<cool::Float>();
}


void
TestParametersCool::copyTo( cool::Record& record ) const
{
  record[ "valueA" ].setValue<cool::Float>(  m_valueA );
  record[ "valueB" ].setValue<cool::Float>(  m_valueB );
  record[ "valueC" ].setValue<cool::Float>(  m_valueC );
}
