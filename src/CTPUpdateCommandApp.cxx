//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/CTPConditionsUpdate.h"
#include "CoolUtils/CTPUpdateCommand.h"

//#include <boost/detail/basic_pointerbuf.hpp>
#include <boost/lexical_cast/bad_lexical_cast.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>

#include "ers/ers.h"
#include "ers/Issue.h"

#include <iostream>
#include <string>

using namespace std;
using namespace daq::coolutils;

namespace po = boost::program_options;


namespace daq
{
  namespace coolutils
  {
    std::istream& operator>>( std::istream& in, FolderIndex& folderIndex )
    {
      std::string token;
      in >> token;
      if      ( token == "beamspot" )
        folderIndex = BEAMSPOT;
      else if ( token == "luminosity" )
        folderIndex = LUMINOSITY;
      else
        throw boost::program_options::invalid_option_value( token );
      return in;
    }
  }
}


int main( int argc, char* argv[] )
{
  enum { SUCCESS=0, BAD_USAGE=1, BAD_COMMAND=2 };

  FolderIndex folderIndex;
  //  std::string partitionName;
  std::string controllerName;
  bool doNotSend;

  // Should read these defaults from OKS
  const std::string defaultControllerName = "CtpController";

  // Parse the command line
  try
    {
      po::options_description desc("Options");
      desc.add_options()
        ( "folder-index,i", po::value<FolderIndex>( &folderIndex )->required(),
          "conditions folder to be updated. Allowed values: beamspot, luminosity" )
        //        ("partition,p", po::value<std::string>(&partitionName)->default_value(partitionName),
        //         "Name of the partition (TDAQ_PARTITION)")
        ("controller,n", po::value<std::string>(&controllerName)->default_value(defaultControllerName),
         "master trigger (controller) name")
        ( "do-not-send,N", po::bool_switch( &doNotSend ),
          "prepare the command, but do not send it" )
        //    ( "lumiblock,l", po::value<int>( &lbn )->default_value(0), "luminosity block to be used for update" )
        ( "help,h", "this help message" )
        ;

      po::variables_map vm;
      po::store( po::parse_command_line( argc, argv, desc ), vm );

      if ( vm.count("help") )
        {
          cout << desc << endl;
          return BAD_USAGE;
        }

      po::notify( vm );
    }
  catch ( std::exception& e )
    {
      cerr << "Error: " << e.what() << endl;
      return BAD_USAGE;
    }

  try
    {
      // Prepare the command and send it
      CTPUpdateCommand ctpUpdateCommand( controllerName );
      if ( not ctpUpdateCommand.send( folderIndex, doNotSend ) )
        {
          return BAD_COMMAND;
        }
    }
  catch ( ers::Issue& e )
    {
      ers::error(e);
      return BAD_COMMAND;
    }

  return SUCCESS;
}
