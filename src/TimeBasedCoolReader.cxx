//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// This class
#include "CoolUtils/TimeBasedCoolReader.h"
#include "CoolUtils/CoolReaderEngine.h"
#include "CoolKernel/ValidityKey.h"

#include "ers/Issue.h"
#include "ers/ers.h"

#include <ctime>
#include <iomanip>
#include <string>
#include <ostream>


namespace daq { namespace coolutils { class CoolReadable; } }

// Helpers

namespace
{

  void ers_info_time( time_t timestamp )
  {
    ERS_INFO( "Selecting IOV that contains timestamp: " << timestamp
              << ", which is: " << std::put_time( localtime(&timestamp), "%c" )
              );
    // Note that localtime() uses a static buffer and is not thread-safe (and without a C++ alternative)
  }

  void ers_info_time( timespec ts )
  {
    ERS_INFO( "Selecting IOV that contains timestamp: " << ts.tv_sec
              << "." << std::setfill('0') << std::setw(9) << ts.tv_nsec
              << ", which is: " << std::put_time( localtime(&ts.tv_sec), "%c" )
              << " and " << ts.tv_nsec << " ns" );
    // localtime() is not thread-safe
  }

  cool::ValidityKey validityKey( time_t timestamp )
  {
    // The validity key is 63 bits with ns precision, timestamp is in seconds since the epoch
    const cool::ValidityKey key = cool::ValidityKey(timestamp) * cool::ValidityKey(1000000000);
    ERS_DEBUG( 0, "Computed validity key from timestamp as: since=" << key );
    return key;
  }

  cool::ValidityKey validityKey( timespec ts )
  {
    // The validity key is 63 bits with ns precision
    const cool::ValidityKey key = cool::ValidityKey(ts.tv_sec) * cool::ValidityKey(1000000000)
      + cool::ValidityKey(ts.tv_nsec);
    ERS_DEBUG( 0, "Computed validity key from timestamp as: since=" << key );
    return key;
  }
  
}


// Constructors

daq::coolutils::TimeBasedCoolReader::TimeBasedCoolReader( const std::string& connectionName,
                                                          const std::string& folderName,
                                                          const std::string& tagName )
  : m_reader( connectionName, folderName, tagName, true )
    // The last argument means time-based (as opposed to run-lumi based)
{
  ERS_INFO( "Instantiated a TimeBasedCoolReader for"
            << " connection \"" << connectionName << "\""
            << ", folder \"" << folderName << "\""
            << ", and tag \"" << tagName << "\"" );
}

// Destructor

daq::coolutils::TimeBasedCoolReader::~TimeBasedCoolReader()
{
}

// Methods

// bool
// daq::coolutils::TimeBasedCoolReader::read( CoolReadable& object,
//                                            time_t timestamp,
//                                            unsigned int channel ) const
// {
//   // Assume nanoseconds as zero
//   std::timespec ts(timestamp,0);
//   return read( object, ts, channel );
// }

bool
daq::coolutils::TimeBasedCoolReader::read( CoolReadable& object,
                                           time_t timestamp,
                                           unsigned int channel ) const
{
  ers_info_time( timestamp );
  return m_reader.read( object, validityKey( timestamp ), channel );
}


bool
daq::coolutils::TimeBasedCoolReader::read( CoolReadable& object,
                                           timespec ts,
                                           unsigned int channel ) const
{
  ers_info_time( ts );
  return m_reader.read( object, validityKey( ts ), channel );
}


bool
daq::coolutils::TimeBasedCoolReader::read( std::vector<CoolReadable>& objects,
                                           time_t timestamp) const
{
  ers_info_time( timestamp );
  return m_reader.read( objects, validityKey( timestamp ) );
}


bool
daq::coolutils::TimeBasedCoolReader::read( std::vector<CoolReadable>& objects,
                                           timespec ts ) const
{
  ers_info_time( ts );
  return m_reader.read( objects, validityKey( ts ) );
}


bool
daq::coolutils::TimeBasedCoolReader::read( CoolReadableVector& objects,
                                           time_t timestamp) const
{
  ers_info_time( timestamp );
  return m_reader.read( objects, validityKey( timestamp ) );
}


bool
daq::coolutils::TimeBasedCoolReader::read( CoolReadableVector& objects,
                                           timespec ts ) const
{
  ers_info_time( ts );
  return m_reader.read( objects, validityKey( ts ) );
}
