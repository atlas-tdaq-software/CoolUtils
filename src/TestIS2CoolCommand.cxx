//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "TestIS2CoolCommand.h"

#include "CoolUtils/IS2CoolCommandHandler.h"
#include "TestIS2CoolObject.h"

#include <memory>


// Constructors

daq::coolutils::TestIS2CoolCommand::TestIS2CoolCommand( const std::string& dbConnectionName,
                                                        const std::string& dbFolderName,
                                                        const std::string& dbTagName,
                                                        const std::string& isObjectName )

  : IS2CoolCommand( dbConnectionName,
                    dbFolderName,
                    dbTagName,
                    isObjectName )
{
}

// Destructor

daq::coolutils::TestIS2CoolCommand::~TestIS2CoolCommand()
{
}

// Methods

bool
daq::coolutils::TestIS2CoolCommand::execute( unsigned int runNumber,
                                             unsigned int lumiBlockNumber )
{
  // Create a test object and transfer it via the handler
  TestIS2CoolObject testObject;

  const bool success = handler()->transfer( runNumber, lumiBlockNumber, testObject );
  return success;
}
