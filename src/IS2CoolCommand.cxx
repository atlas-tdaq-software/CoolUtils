//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/IS2CoolCommand.h"
#include "CoolUtils/IS2CoolCommandHandler.h"

#include "ers/ers.h"
#include "ers/Issue.h"

#include <ostream>


// Constructors

daq::coolutils::IS2CoolCommand::IS2CoolCommand( const std::string& dbConnectionName,
                                                const std::string& dbFolderName,
                                                const std::string& dbTagName,
                                                const std::string& isObjectName )

  : m_handlerPtr( new IS2CoolCommandHandler( dbConnectionName,
                                             dbFolderName,
                                             dbTagName,
                                             isObjectName ) )
{
}

// Destructor

daq::coolutils::IS2CoolCommand::~IS2CoolCommand()
{
}

// Methods

bool
daq::coolutils::IS2CoolCommand::execute( unsigned int runNumber,
                                         unsigned int lumiBlockNumber )
{
  ERS_INFO( "Executing dummy command handler for run " << runNumber
            << " and lumi block " << lumiBlockNumber << " ..." );
  return false;
}
