//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// This class
#include "TestIS2CoolObject.h"

#include <CoolKernel/RecordSpecification.h>
#include <CoolKernel/Record.h>
#include <CoolKernel/IField.h>
#include <CoolKernel/StorageType.h>
#include <CoolKernel/types.h>

#include "daq/coolutils/TestParametersNamed.h"
#include "ipc/partition.h"

#include <string>
#include <memory>


namespace daq
{
  namespace coolutils
  {

    ISNamedInfo*
    TestIS2CoolObject::create( const IPCPartition& partition, const std::string& objectName ) const
    {
      return new daq::coolutils::TestParametersNamed( partition, objectName );
    }


    void
    TestIS2CoolObject::fillRecord( cool::Record& record ) const
    {
      // Is there a better way to do the down cast?
      const TestParametersNamed& isObject = static_cast< const TestParametersNamed& >( *isNamedInfoPtr() );
      record[ "valueA" ].setValue<cool::Float>( isObject.valueA );
      record[ "valueB" ].setValue<cool::Float>( isObject.valueB );
      record[ "valueC" ].setValue<cool::Float>( isObject.valueC );
    }


    void
    TestIS2CoolObject::setRecordSpec( cool::RecordSpecification& recordSpec ) const
    {
      recordSpec.extend( "valueA", cool::StorageType::Float );
      recordSpec.extend( "valueB", cool::StorageType::Float );
      recordSpec.extend( "valueC", cool::StorageType::Float );
    }

  }
}
