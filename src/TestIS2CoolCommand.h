//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_TESTIS2COOLCOMMAND_H
#define DAQ_COOLUTILS_TESTIS2COOLCOMMAND_H

#include "CoolUtils/IS2CoolCommand.h"

#include <string>


namespace daq
{
  namespace coolutils
  {
    // This is the IS object name used for the in-run conditions updates of the HLT.
    const std::string defaultObjectName     = "CoolUtils.TestParameters";

    // This is the DB connection used for the in-run conditions updates of the HLT.
    const std::string defaultConnectionName = "COOLONL_TEST/CONDBR2";
    const std::string defaultFolderName     = "/CoolUtils/TestParameters";
    const std::string defaultTagName        = "CoolUtilsTestParameters-HLT-UPD1-001-00";

    class TestIS2CoolCommand : public IS2CoolCommand
    {
    public:
      // Constructors
      TestIS2CoolCommand( const std::string& dbConnectionName = defaultConnectionName,
                          const std::string& dbFolderName     = defaultFolderName,
                          const std::string& dbTagName        = defaultTagName,
                          const std::string& isObjectName     = defaultObjectName );

      // Destructor
      virtual ~TestIS2CoolCommand();

      // Methods
      bool execute( unsigned int runNumber, unsigned int lumiBlockNumber );

    private:
      // Data members
    };

  }
}

#endif // DAQ_COOLUTILS_TESTIS2COOLCOMMAND_H
