//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/CoolReadable.h"
#include "CoolUtils/CoolWritable.h"

#include <CoolKernel/IObject.h>
#include <CoolKernel/IRecord.h>
#include <CoolKernel/Record.h>
#include <CoolKernel/RecordSpecification.h>
#include <CoolKernel/pointers.h>

#include "ers/ers.h"

#include <memory>

// Constructor

daq::coolutils::CoolReadable::CoolReadable()
{
}

// Destructor

daq::coolutils::CoolReadable::~CoolReadable()
{
}

// Operators
//
// Compare the payloads between a read and a written object
// Useful when testing writing an object followed by reading it back

bool daq::coolutils::operator==( const CoolReadable& cr, const CoolWritable& cw )
{
  // If nothing has been read, the comparison is always false
  if ( ! cr.object() ) return false;

  // Reconstruct a record from the writable
  cool::RecordSpecification recordSpec;
  cw.setRecordSpec( recordSpec );  // Derived class method
  cool::Record record( recordSpec );
  cw.fillRecord( record );   // Method implemented in the derived class

  ERS_DEBUG(0, "lhs: " << cr.object()->payload() );
  ERS_DEBUG(0, "rhs: " << record );

  // An IObject payload is an IRecord
  return ( cr.object()->payload() == record );
}

bool daq::coolutils::operator!=( const CoolReadable& cr, const CoolWritable& cw )
{
  return ! ( cr == cw );
}


bool daq::coolutils::operator==( const CoolReadable& cr1, const CoolReadable& cr2 )
{
  // If these point to the same object or are both zero, the comparison is true
  if ( cr1.object() == cr2.object() )
    return true;

  // If only one of the them is zero the comparison is always false
  if ( ! cr1.object() || ! cr2.object() )
    return false;

  // Else, compare the payloads
  return ( cr1.object()->payload() == cr2.object()->payload() );
}


bool daq::coolutils::operator!=( const CoolReadable& cr1, const CoolReadable& cr2 )
{
  return ! ( cr1 == cr2 );
}


// Private constructor

daq::coolutils::CoolReadable::CoolReadable( cool::IObjectPtr objectPtr )
  : m_objectPtr ( objectPtr )
{
}
