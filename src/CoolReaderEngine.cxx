//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/CoolReaderEngine.h"
#include "CoolUtils/CoolReadable.h"
#include "CoolUtils/CoolReadableVector.h"

#include <CoolApplication/DatabaseSvcFactory.h>
#include <CoolKernel/Exception.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/ValidityKey.h>
#include <CoolKernel/pointers.h>
#include <CoolKernel/ChannelId.h>
#include <CoolKernel/ChannelSelection.h>
#include <CoolKernel/IRecord.h>

#include "ers/ers.h"
#include "ers/Issue.h"
#include "ers/LocalContext.h"
#include "ers/Assertion.h"

#include <memory>
#include <ostream>
#include <string>
#include <iterator>
#include <algorithm>
#include <cstddef>


// Custom ERS exceptions

namespace daq 
{
  ERS_DECLARE_ISSUE(	coolutils,
			BadCoolRead,
			"Reading from COOL failed because: " << explanation,
                        ( (const char*) explanation )
                        )
}

// Constructors

daq::coolutils::CoolReaderEngine::CoolReaderEngine( const std::string& connectionName,
                                                    const std::string& folderName,
                                                    const std::string& tagName,
                                                    bool timebased )
  : m_connectionName( connectionName )
  , m_folderName( folderName )
  , m_tagName( tagName )
  , m_timebased( timebased )
  , m_folder( nullptr )
{
  ERS_INFO( "Instantiated a CoolReaderEngine for"
            << " connection \"" << m_connectionName << "\""
            << ", folder \"" << m_folderName << "\""
            << ", and tag \"" << m_tagName << "\"" );

  ERS_DEBUG( 0, "Reader is " << ( timebased ? "time" : "run-lumi" ) << " based" );
}

// Destructor

daq::coolutils::CoolReaderEngine::~CoolReaderEngine()
{
}

// Methods

bool
daq::coolutils::CoolReaderEngine::read( CoolReadable& object,
                                        cool::ValidityKey from,
                                        unsigned int channel ) const
{
  ERS_INFO( "Reading from COOL"
            << " database \"" << m_connectionName << "\""
            << " and folder \"" << m_folderName << "\""
            << "\" with tag \"" << m_tagName << "\"" );

  ERS_INFO( "Selecting IOV that contains validity key: " << from );

  ERS_INFO( "Using channel: " << channel );

  try
    {
      // See if we have a valid folder pointer from a previous call to read(), otherwise try to get one
      // FIXME: see below
      //      if ( ! m_folder )
      if ( ! getFolder() )
        return false;
      //      else
      //          ERS_DEBUG( 0, "Accessing previously opened folder: " << m_folderName );
      
      ERS_ASSERT( m_folder );
      
      const cool::ChannelId channelId = channel;

      try
        {
          // Fetch the object for the given time
          ERS_INFO( "Looking for object with tag \"" << m_tagName << "\"" );

          cool::IObjectPtr foundObject = m_folder->findObject( from, channelId, m_tagName );

          if ( ! foundObject )
            {
              daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to find object" );
              ers::error(i);
              return false;
            }

          ERS_INFO( "Found object:" << foundObject->payload() );

          // Save the object smart pointer
          object.set( foundObject );
          //          ERS_INFO( "Found object:" << object.object()->payload() );
        }
      catch ( cool::TagNotFound& e )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to find tag" );
          ers::error(i);
          return false;
        }
      catch ( cool::ObjectNotFound& e )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to find object" );
          ers::error(i);
          return false;
        }
    }
  catch ( std::exception& e )
    {
      // COOL, CORAL and POOL exceptions inherit from std exceptions,
      // so catching std::exception will catch all errors from COOL,
      // CORAL and POOL.
      daq::coolutils::BadCoolRead i( ERS_HERE, e.what() );
      ers::error(i);
      // FIXME: close database?
      return false;
    }

  return true;
}


bool
daq::coolutils::CoolReaderEngine::read( std::vector<CoolReadable>& objectVector,
                                        cool::ValidityKey from ) const
{
  CoolReadableVector readable;

  // Read the objects into a vector pointer
  if ( ! read( readable, from ) )
       return false;

  ERS_ASSERT( readable.objects() );
  const cool::IObjectVector& v = *readable.objects();
    
  // Now move the contents of the objects vector into the caller's std vector
  objectVector.resize( v.size() );
  for ( size_t i=0; i<objectVector.size(); ++i )
    {
      const cool::IObjectPtr o = v[i];
      if ( ! o )
        ERS_DEBUG( 0, "Object pointer is null" );
      else
        ERS_DEBUG( 0, "Copying object pointer for channel: " << o->channelId() );
      // ERS_DEBUG( 0, "Found object: " << o );

      objectVector[i].set( v[i] );
    }
  // FIXME: there's got to be a less pedestrian (faster) way to do this
  return true;
}


bool
daq::coolutils::CoolReaderEngine::read( CoolReadableVector& objects,
                                        cool::ValidityKey from ) const
{
  ERS_INFO( "Reading from COOL"
            << " database \"" << m_connectionName << "\""
            << " and folder \"" << m_folderName << "\""
            << "\" with tag \"" << m_tagName << "\"" );

  ERS_INFO( "Selecting all channels with an IOV that contains validity key: " << from );

  try
    {
      // See if we have a valid folder pointer from a previous call to read(), otherwise try to get one
      //
      // FIXME: If we hold on to the database session we won't see what gets written between consecutive calls to this
      // method. Avoid this for now.
      //      if ( ! m_folder )

      if ( ! getFolder() )
          return false;

      ERS_ASSERT( m_folder );

      // The default selection is to select IOVs from *all* channels
      // with the default order 'order by channel, since'. (There is only IOV per channel since when using a pointInTime.)
      const cool::ChannelSelection allChannels;

      try
        {
          // Retrieve all channels of this folder
          const std::vector<cool::ChannelId> folderChannels = m_folder->listChannels();
          ERS_INFO( "The folder contains " << folderChannels.size() << " channels" );

          {
            std::stringstream ss;
            std::copy( folderChannels.begin(), folderChannels.end(), std::ostream_iterator<unsigned int>(ss," ") );
            ERS_DEBUG( 0, "Channel list: " << ss.str() );
          }

          // FIXME: check if there were zero channels

          // Fetch the object for the given time
          ERS_INFO( "Looking for objects with tag \"" << m_tagName << "\"" );

          cool::IObjectIteratorPtr foundObjects = m_folder->findObjects( from, allChannels, m_tagName );

          if ( ! foundObjects )
            {
              daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to find any objects" );
              ers::error(i);
              return false;
            }

          ERS_INFO( "Found objects for " << foundObjects->size() << " channels" );

          const int missing = folderChannels.size() - foundObjects->size();
          if ( missing )
            {
              ERS_INFO( "No object was found for " << missing << " channels" );
            }
          
          cool::IObjectVectorPtr objectsVector = foundObjects->fetchAllAsVector();
          if ( ! objectsVector )
            {
              daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to fetch all channels as vector" );
              ers::error(i);
              return false;
            }
            
          ERS_DEBUG( 0, "Retrieved vector with " << objectsVector->size() << " channels" );
          if ( objectsVector->size() != foundObjects->size() )
            {
              daq::coolutils::BadCoolRead i( ERS_HERE, "Wrong number of channels retrieved as vector" );
              ers::error(i);
              return false;
            }
          // FIXME: we could check here if any of the object pointers in the vector is null

          // Save the object vector smart pointer
          objects.set( objectsVector );
        }
      catch ( cool::TagNotFound& e )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to find tag" );
          ers::error(i);
          return false;
        }
      catch ( cool::ObjectNotFound& e )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to find object" );
          ers::error(i);
          return false;
        }
    }
  catch ( std::exception& e )
    {
      // COOL, CORAL and POOL exceptions inherit from std exceptions,
      // so catching std::exception will catch all errors from COOL,
      // CORAL and POOL.
      daq::coolutils::BadCoolRead i( ERS_HERE, e.what() );
      ers::error(i);
      // FIXME: close database?
      return false;
    }

  return true;
}



// Private methods

bool daq::coolutils::CoolReaderEngine::getFolder() const
{
  ERS_INFO( "Reading from COOL"
            << " database \"" << m_connectionName << "\""
            << " and folder \"" << m_folderName << "\""
            << "\" with tag \"" << m_tagName << "\"" );

  try
    {
      // Open or create the database
      cool::IDatabasePtr database;

      try 
        {
          ERS_INFO( "Trying to open database \"" << m_connectionName << "\"" );

          const bool read_only = true;
          database = cool::DatabaseSvcFactory::databaseService().openDatabase( m_connectionName, read_only );
        }
      catch ( cool::DatabaseDoesNotExist& e )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Database does not exist" );
          ers::error(i);
          return false;
        }

      if ( ! database )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to open database" );
          ers::error(i);
          return false;
        }

      ERS_DEBUG(0, "Successfully opened database" );

      // Find the folder
      ERS_INFO( "Looking for folder \"" << m_folderName << "\"" );

      if ( ! database->existsFolder( m_folderName ) )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Folder does not exist" );
          ers::error(i);
          return false;
        }

      // Fetch the folder
      //      cool::IFolderPtr folder = database->getFolder( m_folderName );
      m_folder = database->getFolder( m_folderName );

      if ( ! m_folder )
        {
          daq::coolutils::BadCoolRead i( ERS_HERE, "Failed to get folder" );
          ers::error(i);
          return false;
        }

      ERS_DEBUG(0, "Retrieved folder \"" << m_folderName << "\"" );

      // Check if the folder conforms with what we expect
      const bool timebased = ( m_folder->description().find("<timeStamp>time</timeStamp>") != std::string::npos );
      if ( timebased != m_timebased )
        {
          ERS_INFO( "Folder appears to be " << (timebased ? "time" : "run-lumi") << " based while this Reader is not" );

          daq::coolutils::BadCoolRead i( ERS_HERE, "Incomptabile CoolReader for this folder type" );
          ers::error(i);
          return false;
        }
      // FIXME: The above check is shared with CoolWriterEngine and should be made common
    }
  catch ( std::exception& e )
    {
      // COOL, CORAL and POOL exceptions inherit from std exceptions,
      // so catching std::exception will catch all errors from COOL,
      // CORAL and POOL.
      daq::coolutils::BadCoolRead i( ERS_HERE, e.what() );
      ers::error(i);
      // FIXME: close database?
      return false;
    }

  return true;
}
