//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/CoolReadableVector.h"
#include "CoolUtils/CoolReadable.h"

#include "CoolKernel/pointers.h"

#include "ers/ers.h"
#include "ers/Issue.h"
#include "ers/LocalContext.h"

#include <memory>
#include <vector>
#include <ostream>


// Custom ERS exception

namespace daq 
{
  ERS_DECLARE_ISSUE( coolutils,
                     BadCoolAccess,
                     "Accessing COOL data failed because: " << explanation,
                     ( (const char*) explanation )
                     )
}

// Destructor

daq::coolutils::CoolReadableVector::~CoolReadableVector()
{
}


// Methods

daq::coolutils::CoolReadable
daq::coolutils::CoolReadableVector::object( size_t i ) const
{
  // We do a range check here
  if ( i >= m_objectVectorPtr->size() )
    {
      daq::coolutils::BadCoolAccess issue( ERS_HERE, "Index out of range" );
      ers::error(issue);
      return CoolReadable( 0 );
    }

  cool::IObjectPtr objectPtr = m_objectVectorPtr->at( i );
  return CoolReadable( objectPtr );
}
