//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/ISReadable.h"

#include "ipc/partition.h"
#include "is/namedinfo.h"  // iwyu doesn't think we need this but we do

#include <string>


// Destructor

daq::coolutils::ISReadable::~ISReadable()
{
}

// Methods

void
daq::coolutils::ISReadable::initialize( const IPCPartition& partition, const std::string& objectName )
{
  m_payloadPtr.reset( this->create( partition, objectName ) );
}
