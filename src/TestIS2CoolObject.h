//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_TESTIS2COOLOBJECT_H
#define DAQ_COOLUTILS_TESTIS2COOLOBJECT_H

#include "CoolUtils/IS2CoolTransferrable.h"

#include <string>

namespace cool { class Record; }
namespace cool { class RecordSpecification; }

class IPCPartition;
class ISNamedInfo;


namespace daq
{
  namespace coolutils
  {
    class TestIS2CoolObject : public IS2CoolTransferrable
    {
    public:
      // Constructor

      // Destructor
      virtual ~TestIS2CoolObject() {}

    protected:
      // This method is implemented for being ISReadable
      virtual ISNamedInfo* create( const IPCPartition& partition, const std::string& objectName ) const;

      // These methods are implemented for being CoolWritable
      virtual void fillRecord( cool::Record& record ) const;
      virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const;

    private:
      // Data members
    };

  }
}

#endif // DAQ_COOLUTILS_TESTIS2COOLOBJECT_H
