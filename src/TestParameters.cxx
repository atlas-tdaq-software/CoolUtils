//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/TestParameters.h"

//#include <boost/detail/basic_pointerbuf.hpp>
#include <boost/lexical_cast.hpp>
using boost::lexical_cast;

#include <cstdlib>
using std::atof;
using std::atoi;

#include <vector>
#include <string>
using std::vector;
using std::string;

using namespace daq;


void 
TestParameters::copyFrom( int argc, char* argv[] )
{
  // const vector< string > args( argv, argv + argc );
  if ( argc > 1 )
    {
      const vector< string > args( argv + 1, argv + argc );
      copyFrom( args );
    }
}


void 
TestParameters::copyFrom( const vector<string>& pars )
{
  const int n = pars.size();
  int i = -1;
  do
    {
      if ( ++i < n ) m_valueA = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n ) m_valueB = lexical_cast<double>( pars[i] ); else break;
      if ( ++i < n ) m_valueC = lexical_cast<double>( pars[i] ); else break;
    } while (false);
}


std::ostream& operator<<( std::ostream& os, const daq::TestParameters& p )
{
  os
    << "  m_valueA=" << p.valueA()
    << "  m_valueB=" << p.valueB()
    << "  m_valueC=" << p.valueC()
    ;
  return os;
}


bool TestParameters::isValid()
{
  return
       m_valueA > 0.
    && m_valueB > 0.
    && m_valueC > 0.;
}
