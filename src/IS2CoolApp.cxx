//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "TestIS2CoolCommand.h"

#include <cstdlib>
using std::atoi;

#include <string>

// DB and IS connection parameters (defaults)

std::string dbConnection = "sqlite://;schema=ocuIS2CoolApp_test.db;dbname=CONDBR2";
std::string dbFolder     = "/CoolUtils/TestParameters";
std::string dbTag        = "CoolUtils-HLT-UPD1-000-00";
std::string isObject     = "Coolutils.testParameters";


int main( int argc, char* argv[] )
{
  enum { SUCCESS=0, ERROR_RETURN=1, ERROR_EXCEPTION=2 };

  unsigned int run=0, lbn=0;

  int i=0;
  if (argc > ++i) run    = atoi( argv[i] );
  if (argc > ++i) lbn    = atoi( argv[i] );
  if (argc > ++i) dbConnection = argv[i];
  if (argc > ++i) dbFolder     = argv[i];
  if (argc > ++i) dbTag        = argv[i];
  if (argc > ++i) isObject     = argv[i];

  bool success = false;

  try
    {
      daq::coolutils::TestIS2CoolCommand command( dbConnection, dbFolder, dbTag, isObject );
      success = command.execute( run, lbn );
    }
  catch ( std::exception& e )
    {
      return ERROR_EXCEPTION;
    }

  if ( ! success )
    return ERROR_RETURN;

  return SUCCESS;
}
