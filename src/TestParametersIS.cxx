//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/TestParametersIS.h"

#include "daq/coolutils/TestParametersNamed.h"

using namespace daq;

// Methods

void 
TestParametersIS::copyFrom( const daq::coolutils::TestParametersNamed& namedInfo )
{
  m_valueA = namedInfo.valueA;
  m_valueB = namedInfo.valueB;
  m_valueC = namedInfo.valueC;
}


void
TestParametersIS::copyTo( daq::coolutils::TestParametersNamed& namedInfo ) const
{
  namedInfo.valueA = m_valueA;
  namedInfo.valueB = m_valueB;
  namedInfo.valueC = m_valueC;
}

