//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// This class
#include "CoolUtils/ISWriter.h"
#include "CoolUtils/TestParametersIS.h"
#include "daq/coolutils/TestParametersNamed.h"

#include "ers/ers.h"
#include "ers/Issue.h"
#include "ers/LocalContext.h"
#include "ipc/core.h"
#include "ipc/exceptions.h"
#include "ipc/partition.h"

#include <cstdlib>
#include <string>
#include <ostream>                                      // for operator<<


namespace daq { class TestParameters; }

// Custom ERS declarations

namespace daq 
{
  ERS_DECLARE_ISSUE(	coolutils,
			BadISWrite,
			"Writing to IS failed because: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	coolutils,
			InvalidPartition,
			"Partition is not valid: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	coolutils,
			EnvironmentMissing,
			"Environment variable is not defined: " << explanation,
                        ( (const char*) explanation )
                        )
}

// Constructors

daq::coolutils::ISWriter::ISWriter( const std::string& objectName )
  : m_objectName( objectName )
{
  try
    {
      int argc=1;
      const char* argv[] = { "ISWriter" };

      // FIXME: Need to cast away const-ness because IPCCore::init()
      // signature is not const-clean
      IPCCore::init( argc, (char**) argv );
    }
  catch ( daq::ipc::CannotInitialize& e )
    {
      ers::error(e);
      return;
    }
  catch ( daq::ipc::AlreadyInitialized& e )
    {
      // This should be harmless
      ers::log(e);
    }

  // Retrieve TDAQ_PARTITION from the environment
  const char* partitionName = getenv( "TDAQ_PARTITION" );
  if ( partitionName )
    {
      m_partitionName = partitionName;
    }
  else
    {
      daq::coolutils::EnvironmentMissing i( ERS_HERE, "TDAQ_PARTITION" );
      ers::error(i);
      return;
    }
}

// Destructor

daq::coolutils::ISWriter::~ISWriter()
{
}

// Methods

bool
daq::coolutils::ISWriter::write( const TestParameters& parameters )
{
  ERS_INFO( "Trying to connect to partition \"" << m_partitionName << "\"" );

  IPCPartition partition( m_partitionName );

  if ( ! partition.isValid() )
    {
      daq::coolutils::InvalidPartition i( ERS_HERE, m_partitionName.c_str() );
      ers::error(i);
      return false;
    }

  ERS_DEBUG( 0, "Successfully connected to partition \"" << partition.name() << "\"" );

  try
    {
      ERS_INFO( "Trying to write test parameters to IS object \"" << m_objectName << "\"" );
      //      ERS_INFO( "TestParameters:" << parameters );

      daq::coolutils::TestParametersNamed testInfo( partition, m_objectName );

      const TestParametersIS parametersIS( parameters );
      parametersIS.copyTo( testInfo );

      testInfo.checkin();

      ERS_INFO( "Wrote the following test parameters to IS: " << testInfo );
    }
  catch ( ers::Issue& i )
    {
      ers::error(i);
      return false;
    }

  return true;
}
