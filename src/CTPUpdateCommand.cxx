//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#include "CoolUtils/CTPUpdateCommand.h"
#include "TriggerCommander/Exceptions.h"
#include "TriggerCommander/TriggerCommander.h"
#include "TTCInfo/LumiBlockNamed.h"

#include "ers/ers.h"
#include "ers/Issue.h"
#include "ers/LocalContext.h"
#include "ipc/core.h"
#include "ipc/exceptions.h"
#include "ipc/partition.h"

#include <cstdlib>
#include <string>
#include <sstream>

using namespace std;


namespace daq
{
  ERS_DECLARE_ISSUE(	coolutils,
			InvalidPartition,
			"Partition is not valid: " << explanation,
                        ( (const char*) explanation )
                        )

  ERS_DECLARE_ISSUE(	coolutils,
			EnvironmentMissing,
			"Environment variable is not defined: " << explanation,
                        ( (const char*) explanation )
                        )
}


// Constructors

daq::coolutils::CTPUpdateCommand::CTPUpdateCommand( const std::string& controllerName,
                                                    const std::string& isObjectName )
  : m_controllerName( controllerName )
  , m_isObjectName  ( isObjectName )
{
}

// Destructor

daq::coolutils::CTPUpdateCommand::~CTPUpdateCommand()
{
}

// Methods

bool
daq::coolutils::CTPUpdateCommand::send( unsigned int folderIndex, bool doNotSend )
{
  // Initialize IPC
  try
    {
      int argc=1;
      const char* argv[] = { "CTPUpdateCommand" };

      // FIXME: Need to cast away const-ness because IPCCore::init()
      // signature is not const-clean
      IPCCore::init( argc, (char**) argv );
    }
  catch ( daq::ipc::CannotInitialize& e )
    {
      ers::error(e);
      return false;
    }
  catch( daq::ipc::AlreadyInitialized& e)
    {
      ers::warning(e);
    }

  // Retrieve name of partition from the environment
  const char* partitionName = getenv( "TDAQ_PARTITION" );
  if ( ! partitionName )
    {
      daq::coolutils::EnvironmentMissing i( ERS_HERE, "TDAQ_PARTITION" );
      ers::error(i);
      return false;
    }

  ERS_LOG( "Trying to connect to partition \"" << partitionName << "\"" );

  // Connect to partition
  IPCPartition partition( partitionName );

  if ( ! partition.isValid() )
    {
      daq::coolutils::InvalidPartition i( ERS_HERE, partitionName );
      ers::error(i);
      return false;
    }

  try
    {
      // Read the current luminosity block number from IS
      LumiBlockNamed lumiBlock( partition, m_isObjectName );
      lumiBlock.checkout();

      ERS_LOG( "Read the following luminosity info from IS: " << lumiBlock );

      // Choose the next lumi block (there is a race condition here that is harmless)
      const int nextLBN = lumiBlock.LumiBlockNumber + 1;

      if ( ! doNotSend )
        {
          // FIXME: the folderIndex should be checked against what is stored in the the TRIGGER/COOLUPDATE folder as the
          // index of folder "/Indet/Onl/Beampos" or ".../HLTPrefLumi"

          // Send the command
          ERS_LOG( "Sending conditions update command to controller \""
                    << m_controllerName << "\" with folder index " << folderIndex << " and LBN " << nextLBN );

          daq::trigger::TriggerCommander commander( partition, m_controllerName );
          try {
            commander.setConditionsUpdate( folderIndex, nextLBN );
          } catch (daq::trigger::CommandFailed const& exc) {
            // Until Oct 2024 CTP did not raise exceptions for certain errors
            // like requesting multiple updates for the same LB. For this
            // sort of expected errors we do not want to produce ERS errors,
            // just print a message and return false. In the future we may want
            // to handle more specific cases.
            ERS_LOG("Failed to request condition update:");
            ers::log(exc);
          }
        }
      else
        {
          ERS_LOG( "Command not sent per user request" );
        }
    }
  catch ( ers::Issue& e )
    {
      ers::error(e);
      return false;
    }

  return true;
}
