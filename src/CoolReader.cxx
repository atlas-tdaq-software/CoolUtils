//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

// This class
#include "CoolUtils/CoolReader.h"
#include "CoolUtils/CoolReaderEngine.h"
#include "CoolKernel/ValidityKey.h"

#include "ers/ers.h"
#include "ers/Issue.h"

#include <cstdint>
#include <string>
#include <ostream>

namespace daq { namespace coolutils { class CoolReadable; } }


// Constructors

daq::coolutils::CoolReader::CoolReader( const std::string& connectionName,
                                        const std::string& folderName,
                                        const std::string& tagName )
  : m_reader( connectionName, folderName, tagName, false )
    // Last argument means not time-based (i.e. run-lumi based)
{
  ERS_INFO( "Instantiated a CoolReader for"
            << " connection \"" << connectionName << "\""
            << ", folder \"" << folderName << "\""
            << ", and tag \"" << tagName << "\"" );
}

// Destructor

daq::coolutils::CoolReader::~CoolReader()
{
}

// Methods

bool
daq::coolutils::CoolReader::read( CoolReadable& object,
                                  unsigned int runNumber,
                                  unsigned int lumiBlockNumber,
                                  unsigned int channel ) const
{
  ERS_INFO( "Selecting IOV that contains run number: " << runNumber
            << ", lumi block number: " << lumiBlockNumber );

  // Index IOV by run number and lumi block
  const uint64_t a = runNumber;
  const cool::ValidityKey since = ((a<<32) | lumiBlockNumber);

  ERS_INFO( "Computed validity key from run and lumi block number as: since=" << since );

  return m_reader.read( object, since, channel );
}
