//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_CTPUPDATECOMMAND_H
#define DAQ_COOLUTILS_CTPUPDATECOMMAND_H

#include <string>

namespace daq
{
  namespace coolutils
  {
    // The name of the RC controller to send the user command to
    const std::string defaultController = "CtpController";

    // The name of the IS object that holds the current lumi block number
    const std::string defaultISObject   = "RunParams.LumiBlock";

    class CTPUpdateCommand
    {
    public:
      // Constructors
      CTPUpdateCommand( const std::string& controllerName = defaultController,
                        const std::string& isObjectName   = defaultISObject );

      // Destructor
      ~CTPUpdateCommand();

      // Methods

      // If doNotSend==true, the method will prepare and display the command, but not send it
      bool send( unsigned int folderIndex, bool doNotSend = false );

    private:
      // Data members
      std::string m_controllerName;
      std::string m_isObjectName;
    };

  }
}

#endif // DAQ_COOLUTILS_CTPUPDATECOMMAND_H
