//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_COOLREADERENGINE_H
#define DAQ_COOLUTILS_COOLREADERENGINE_H

#include <string>
#include <vector>

#include "CoolKernel/ValidityKey.h"
#include "CoolKernel/pointers.h"

//#include "CoolUtils/CoolReadable.h"
//#include "CoolUtils/CoolReadableVector.h"


namespace daq
{
  namespace coolutils
  {
    class CoolReadable;
    class CoolReadableVector;

    class CoolReaderEngine
    {
    private:
      // This engine can only be instantiated by the interface classes
      friend class CoolReader;
      friend class TimeBasedCoolReader;
      
      // Constructors
      CoolReaderEngine( const std::string& connectionName, const std::string& folderName, const std::string& tagName,
                        bool timebased = false /* , bool multichannel = false */ );

      // Destructor
      ~CoolReaderEngine();

      // Methods
      bool read( CoolReadable& object, 
                 cool::ValidityKey from,
                 unsigned int channel = 0) const;

      bool read( std::vector<CoolReadable>& objects,
                 cool::ValidityKey from ) const;

      bool read( CoolReadableVector& objects,
                 cool::ValidityKey from ) const;

      // A CoolReadable object implements a set() method to capture the IObjectPtr smart pointer

    private:
      // Helper methods
      bool getFolder() const;

      // Data members
      std::string m_connectionName;
      std::string m_folderName;
      std::string m_tagName;
      const bool m_timebased;

      mutable cool::IFolderPtr m_folder;     // cached folder handle
    };

  }
}

#endif // DAQ_COOLUTILS_COOLREADERENGINE_H
