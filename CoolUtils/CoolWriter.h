//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_COOLWRITER_H
#define DAQ_COOLUTILS_COOLWRITER_H

#include <string>

#include "CoolUtils/CoolWriterEngine.h"


namespace daq
{
  namespace coolutils
  {
    class CoolWritable;

    class CoolWriter
    {
    public:
      // Constructors
      CoolWriter( const std::string& connectionName, const std::string& folderName, const std::string& tagName );

      // Destructor
      ~CoolWriter();

      /**
       *  Write open-ended interval from key=(run, lb) to infinity.
       *
       *  This method can be used only to store intervals which "close" existing
       *  open-ended intervals. If folder is locked then it is unlocked and re-locked
       *  after update.
       *
       *  Method checks existing interval which covers (run, lb). If that interval is
       *  closed (its "until" is not infinity) then error message is produced to ERS
       *  and false is returned. If old interval is identical to new interval then
       *  similarly ERS message is produced and false is returned. Any COOL exceptions
       *  are caught and false is returned for them.
       */
      bool write( unsigned int runNumber,
                  unsigned int lumiBlockNumber,
                  const CoolWritable& object,
                  unsigned int channel = 0 ) const;

      /**
       *  Write open-ended interval from key=(run, lb) to infinity.
       *
       *  The difference with write() method is that this method can overwrite existing
       *  interval if its 'since' key is identical to 'since' of the new interval.
       */
      bool overwrite( unsigned int runNumber,
                      unsigned int lumiBlockNumber,
                      const CoolWritable& object,
                      unsigned int channel = 0 ) const;

      // A CoolWritable object implements the following two methods used by this writer:
      //     virtual void fillRecord( cool::Record& record ) const;
      //     virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const;

    private:

      /**
       *  If `overwrite` is true then allow overwriting of existing interval with the
       *  same 'since' value (and 'until'=infinity).
       */
      bool _write( unsigned int runNumber,
                   unsigned int lumiBlockNumber,
                   const CoolWritable& object,
                   unsigned int channel,
                   bool overwrite ) const;

      // Data members
      CoolWriterEngine m_writer;
    };

  }
}

#endif // DAQ_COOLUTILS_COOLWRITER_H
