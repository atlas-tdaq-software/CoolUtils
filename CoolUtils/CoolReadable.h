//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_COOLREADABLE_H
#define DAQ_COOLUTILS_COOLREADABLE_H

#include <CoolKernel/pointers.h>

namespace daq
{
  namespace coolutils
  {
    class CoolReadable
    {
    public:
      // Constructor
      CoolReadable();
      
      // Destructor
      virtual ~CoolReadable();
      
      // Accessor
      virtual cool::IObjectPtr object() const { return m_objectPtr; }

    private:

      friend class CoolReaderEngine;
      friend class CoolReadableVector;

      // Make an individual CoolReadable from an object that was read as part of a vector.
      // Keeping this private still guarantees that a CoolReadable object is always read through the engine.
      CoolReadable( cool::IObjectPtr );
      
      // Methods
      virtual void set( cool::IObjectPtr objectPtr ) { m_objectPtr = objectPtr; }

      // Data members
      cool::IObjectPtr m_objectPtr;
    };

    // Comparisons
    // These two operators compare the two payloads between a read and a written object
    // This is useful when testing writing an object followed by reading it back

    // Forward declarations
    class CoolWritable;

    bool operator==( const CoolReadable& cr, const CoolWritable& cw );
    bool operator!=( const CoolReadable& cr, const CoolWritable& cw );

    // Also compare objects that were both read
    bool operator==( const CoolReadable& r1, const CoolReadable& r2 );
    bool operator!=( const CoolReadable& r1, const CoolReadable& r2 );
  }
}

#endif // DAQ_COOLUTILS_COOLREADABLE_H
