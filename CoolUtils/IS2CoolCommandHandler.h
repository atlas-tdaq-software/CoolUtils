//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_IS2COOLCOMMANDHANDLER_H
#define DAQ_COOLUTILS_IS2COOLCOMMANDHANDLER_H

#include "CoolUtils/ISReader.h"
#include "CoolUtils/CoolWriter.h"

#include <string>


namespace daq
{
  namespace coolutils
  {
    class IS2CoolTransferrable;

    class IS2CoolCommandHandler
    {
    public:
      // Constructors
      IS2CoolCommandHandler( const std::string& dbConnectionName,
                             const std::string& dbFolderName,
                             const std::string& dbTagName,
                             const std::string& isObjectName );

      // Destructor
      ~IS2CoolCommandHandler();

      // Methods
      bool transfer( unsigned int runNumber, unsigned int lumiBlockNumber, IS2CoolTransferrable& object ) const;

    private:
      // Data members
      std::string m_connection;

      ISReader   m_isReader;
      CoolWriter m_coolWriter;
    };
  }
}

#endif // DAQ_COOLUTILS_IS2COOLCOMMANDHANDLER_H
