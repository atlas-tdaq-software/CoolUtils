//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_COOLREADER_H
#define DAQ_COOLUTILS_COOLREADER_H

#include <string>

#include "CoolUtils/CoolReaderEngine.h"

namespace daq
{
  namespace coolutils
  {
    class CoolReadable;

    class CoolReader
    {
    public:
      // Constructors
      CoolReader( const std::string& connectionName,
                  const std::string& folderName,
                  const std::string& tagName );

      // Destructor
      ~CoolReader();

      // Methods
      bool read( CoolReadable& object, 
                 unsigned int runNumber,
                 unsigned int lumiBlockNumber,
                 unsigned int channel = 0) const;

      // A CoolReadable object implements a set() method to capture the IObjectPtr smart pointer

    private:
      // Data members
      CoolReaderEngine m_reader;
    };

  }
}

#endif // DAQ_COOLUTILS_COOLREADER_H
