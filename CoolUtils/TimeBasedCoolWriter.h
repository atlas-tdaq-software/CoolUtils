//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_TIMEBASEDCOOLWRITER_H
#define DAQ_COOLUTILS_TIMEBASEDCOOLWRITER_H

#include <string>
#include <ctime>

#include "CoolUtils/CoolWriterEngine.h"


namespace daq
{
  namespace coolutils
  {
    class CoolWritable;

    class TimeBasedCoolWriter
    {
    public:
      // Constructors
      TimeBasedCoolWriter( const std::string& connectionName, const std::string& folderName, const std::string& tagName );

      // Destructor
      ~TimeBasedCoolWriter();

      /**
       *  Write open-ended interval from the given timestamp to infinity.
       *
       *  This method can be used only to store intervals which "close" existing
       *  open-ended intervals. If folder is locked then it is unlocked and re-locked
       *  after update.
       *
       *  Method checks existing interval which covers (run, lb). If that interval is
       *  closed (its "until" is not infinity) then error message is produced to ERS
       *  and false is returned. If old interval is identical to new interval then
       *  similarly ERS message is produced and false is returned. Any COOL exceptions
       *  are caught and false is returned for them.
       */
      bool write( time_t timestamp,    // seconds since the epoch
                  const CoolWritable& object,
                  unsigned int channel = 0 ) const;

      bool write( timespec ts,    // nanosecond precision
                  const CoolWritable& object,
                  unsigned int channel = 0 ) const;

      // A CoolWritable object implements the following two methods used by this writer:
      //     virtual void fillRecord( cool::Record& record ) const;
      //     virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const;

    private:

      // Data members
      CoolWriterEngine m_writer;
    };

  }
}

#endif // DAQ_COOLUTILS_TIMEBASEDCOOLWRITER_H
