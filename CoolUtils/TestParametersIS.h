//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_TESTPARAMETERS_IS_H
#define DAQ_TESTPARAMETERS_IS_H

// Base class
#include "CoolUtils/TestParameters.h"

namespace daq
{

  namespace coolutils
  {
    // Forward declare IS info object that is auto-generated
    class TestParametersNamed;
  }

  class TestParametersIS : public TestParameters
  {
  public:

    // Constructors
    TestParametersIS( const TestParameters& other )
      : TestParameters( other )
    {}

    TestParametersIS()
      : TestParameters()
    {}

    // Methods
    void copyFrom( const coolutils::TestParametersNamed& namedInfo );
    void copyTo( coolutils::TestParametersNamed& namedInfo ) const;
  };

}

#endif // DAQ_TESTPARAMETERS_IS_H
