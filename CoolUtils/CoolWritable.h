//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_COOLWRITABLE_H
#define DAQ_COOLUTILS_COOLWRITABLE_H

// Forward declarations
namespace cool { class Record; }
namespace cool { class RecordSpecification; }


namespace daq
{
  namespace coolutils
  {
    class CoolWritable
    {
    public:
      // Constructor

      // Destructor
      virtual ~CoolWritable();

      // These two methods have to be implemented by the derived class:

      virtual void fillRecord( cool::Record& record ) const = 0;
      // This method is executed to fill a COOL record from the object that is supposed to be stored.
      // Typically this would be done through a series of assignments like:
      //     record[ "valueA" ].setValue<cool::Int32>( this->valueA );
      //     record[ "valueB" ].setValue<cool::Float>( this->valueB );

      virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const = 0;
      // This method is executed to fill the COOL record specification of the folder that the object is being written to.
      // Typically this would be done through a number of calls like:
      //     recordSpec.extend( "valueA", cool::StorageType::Int32 );
      //     recordSpec.extend( "valueB", cool::StorageType::Float );
    };
  }
}

#endif // DAQ_COOLUTILS_COOLWRITABLE_H
