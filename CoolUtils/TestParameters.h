//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_TESTPARAMETERS_H
#define DAQ_TESTPARAMETERS_H

#include <iostream>
#include <vector>
#include <string>


namespace daq
{

  class TestParameters
  {
  public:

    // Constructor
    TestParameters()
      :  m_valueA(0.)
      ,  m_valueB(0.)
      ,  m_valueC(0.)
    {}

    // Accessors
    float valueA() const { return m_valueA; }
    float valueB() const { return m_valueB; }
    float valueC() const { return m_valueC; }
    
    // Methods
    void copyFrom( int argc, char* argv[] );
    void copyFrom( const std::vector< std::string >& args );
    bool isValid();

  protected:

    // Data members
    float m_valueA;
    float m_valueB;
    float m_valueC;
  };

}

std::ostream& operator<<( std::ostream& os, const daq::TestParameters& parameters );

#endif // DAQ_TESTPARAMETERS_H
