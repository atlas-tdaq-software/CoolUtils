//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_ISREADER_H
#define DAQ_COOLUTILS_ISREADER_H

#include <string>


namespace daq
{
  namespace coolutils
  {
    class ISReadable;

    class ISReader
    {
    public:
      // Constructors
      ISReader( const std::string& objectName );
      // The name of the partition is taken from the environment ($TDAQ_PARTITION)

      // Destructor
      ~ISReader();

      // Methods
      bool read( ISReadable& object ) const;
      // An ISReadable object implements the following factory method that this reader uses:
      //   virtual ISNamedInfo* create( const std::string& partitionName, const std::string& objectName ) const;

    private:
      // Data members
      std::string m_partitionName;
      std::string m_objectName;
    };

  }
}

#endif // DAQ_COOLUTILS_ISREADER_H
