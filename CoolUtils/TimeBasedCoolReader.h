//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_TIMEBASEDCOOLREADER_H
#define DAQ_COOLUTILS_TIMEBASEDCOOLREADER_H

#include <string>
#include <ctime>
#include <vector>

#include "CoolUtils/CoolReaderEngine.h"

namespace daq
{
  namespace coolutils
  {
    class CoolReadable;
    class CoolReadableVector;

    class TimeBasedCoolReader
    {
    public:
      // Constructors
      TimeBasedCoolReader( const std::string& connectionName,
                           const std::string& folderName,
                           const std::string& tagName );

      // Destructor
      ~TimeBasedCoolReader();

      // Methods

      bool read( CoolReadable& object,
                 time_t timestamp,    // seconds since the epoch
                 unsigned int channel = 0) const;

      bool read( CoolReadable& object,
                 timespec ts,    // nanosecond precision
                 unsigned int channel = 0) const;

      // read IOVs for all channels
      bool read( std::vector<CoolReadable>& objects,
                 time_t timestamp    // seconds since the epoch
                 ) const;

      bool read( std::vector<CoolReadable>& objects,
                 timespec ts    // nanosecond precision
                 ) const;

      // read IOVs for all channels
      bool read( CoolReadableVector& objects,
                 time_t timestamp    // seconds since the epoch
                 ) const;

      bool read( CoolReadableVector& objects,
                 timespec ts    // nanosecond precision
                 ) const;

      // A CoolReadable object implements a set() method to capture the IObjectPtr smart pointer

    private:
      // Data members
      CoolReaderEngine m_reader;
    };

  }
}

#endif // DAQ_COOLUTILS_TIMEBASEDCOOLREADER_H
