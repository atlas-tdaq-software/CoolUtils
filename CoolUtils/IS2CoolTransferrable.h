//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_IS2COOLTRANSFERRABLE_H
#define DAQ_COOLUTILS_IS2COOLTRANSFERRABLE_H

#include "CoolUtils/ISReadable.h"
#include "CoolUtils/CoolWritable.h"


namespace daq
{
  namespace coolutils
  {
    class IS2CoolTransferrable : public ISReadable, public CoolWritable
    {
    public:
      // Constructor

      // Destructor
      virtual ~IS2CoolTransferrable() {}

    private:
    };

  }
}

#endif // DAQ_COOLUTILS_IS2COOLTRANSFERRABLE_H
