//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_ISREADABLE_H
#define DAQ_COOLUTILS_ISREADABLE_H

#include <string>
#include <memory>

class IPCPartition;
class ISNamedInfo;


namespace daq
{
  namespace coolutils
  {
    class ISReadable
    {
    public:
      // Constructor

      // Destructor
      virtual ~ISReadable();

      // Accessors
      std::shared_ptr< ISNamedInfo > isNamedInfoPtr() const { return m_payloadPtr; }

      // Methods
      void initialize( const IPCPartition& partition, const std::string& objectName );
      // initialize() must be called before accessing the payload

    protected:
      // The following factory method has to be implemented by the derived class:
      virtual ISNamedInfo* create(  const IPCPartition& partition, const std::string& objectName ) const = 0;

    private:
      // Data members
      std::shared_ptr< ISNamedInfo > m_payloadPtr;
    };
  }
}

#endif // DAQ_COOLUTILS_ISREADABLE_H
