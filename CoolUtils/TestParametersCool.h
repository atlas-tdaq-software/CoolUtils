//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_TESTPARAMETERS_COOL_H
#define DAQ_TESTPARAMETERS_COOL_H

// Base class
#include "CoolUtils/TestParameters.h"

namespace cool { class IRecord; }
namespace cool { class Record; }


namespace daq
{

  class TestParametersCool : public TestParameters
  {
  public:

    // Constructors
    TestParametersCool( const TestParameters& other )
      : TestParameters( other )
    {}

    TestParametersCool()
      : TestParameters()
    {}

    // Methods
    void copyFrom( const cool::IRecord& object );
    void copyTo( cool::Record& record ) const;
  };

}

#endif // DAQ_TESTPARAMETERS_COOL_H
