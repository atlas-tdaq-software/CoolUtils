//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_CTPCONDITIONSUPDATE_H
#define DAQ_COOLUTILS_CTPCONDITIONSUPDATE_H

namespace daq
{
  namespace coolutils
  {
    enum FolderIndex { BEAMSPOT=0, LUMINOSITY=1, HLTPRESCALE=2, FolderIndexMax=HLTPRESCALE };
  }
}

#endif // DAQ_COOLUTILS_CTPCONDITIONSUPDATE_H
