//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_IS2COOLCOMMAND_H
#define DAQ_COOLUTILS_IS2COOLCOMMAND_H

#include <string>
#include <memory>


namespace daq
{
  namespace coolutils
  {
    class IS2CoolCommandHandler;

    class IS2CoolCommand
    {
    public:
      // Constructors
      IS2CoolCommand( const std::string& dbConnectionName,
                      const std::string& dbFolderName,
                      const std::string& dbTagName,
                      const std::string& isObjectName );

      // Destructor
      ~IS2CoolCommand();

      // Methods
      virtual bool execute( unsigned int runNumber, unsigned int lumiBlockNumber );

    protected:
      std::shared_ptr< IS2CoolCommandHandler > handler() { return m_handlerPtr; }

    private:
      // Data members
      std::shared_ptr< IS2CoolCommandHandler > m_handlerPtr;
    };
  }
}

#endif // DAQ_COOLUTILS_IS2COOLCOMMAND_H
