//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_COOLWRITERENGINE_H
#define DAQ_COOLUTILS_COOLWRITERENGINE_H

#include <string>

// Forward declarations
//class cool::Record;
//class cool::RecordSpecification;
//#include <CoolKernel/RecordSpecification.h>
//#include <CoolKernel/Record.h>
#include "CoolKernel/ValidityKey.h"


namespace daq
{
  namespace coolutils
  {
    class CoolWritable;

    class CoolWriterEngine
    {
    private:
      // This engine can only be instantiated by the interface classes
      friend class CoolWriter;
      friend class TimeBasedCoolWriter;

      // Constructors
      CoolWriterEngine( const std::string& connectionName, const std::string& folderName, const std::string& tagName,
                        bool timestamp = false, bool multichannel = false );

      // Destructor
      ~CoolWriterEngine();

      // A CoolWritable object implements the following two methods used by this writer:
      //     virtual void fillRecord( cool::Record& record ) const;
      //     virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const;

      /**
       *  Write open-ended interval starting at validity key 'from' to infinity.
       *
       *  This method can be used only to store intervals which "close" existing
       *  open-ended intervals. If folder is locked then it is unlocked and re-locked
       *  after update.
       *
       *  Method checks existing interval which covers (run, lb). If that interval is
       *  closed (its "until" is not infinity) then error message is produced to ERS
       *  and false is returned. If the old interval is identical to new interval then
       *  similarly ERS message is produced and false is returned. Any COOL exceptions
       *  are caught and false is returned for them.
       *
       * If `overwrite` is true then this method can overwrite an existing interval
       * with the same 'since' value (and 'until'=infinity).
       */
      bool write( cool::ValidityKey from,
                  const CoolWritable& object,
                  unsigned int channel = 0,
                  bool overwrite = false ) const;

      // Data members
      std::string m_connectionName;
      std::string m_folderName;
      std::string m_tagName;
      bool m_timestamp;
      bool m_multichannel;
      mutable bool m_newFolder; // State variable set when creating our own folders
    };

  }
}

#endif // DAQ_COOLUTILS_COOLWRITERENGINE_H
