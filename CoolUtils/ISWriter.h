//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_ISWRITER_H
#define DAQ_COOLUTILS_ISWRITER_H

#include <string>

namespace daq { class TestParameters; }


namespace daq
{
  namespace coolutils
  {

    class ISWriter
    {
    public:
      // Constructors
      ISWriter( const std::string& objectName );

      // Destructor
      ~ISWriter();

      // Methods
      bool write( const TestParameters& parameters );

    private:
      // Data members
      std::string m_partitionName;
      std::string m_objectName;
    };

  }
}

#endif // DAQ_COOLUTILS_ISWRITER_H
