//
// @author Rainer Bartoldus, bartoldu@slac.stanford.edu
// @version $Id$
//

#ifndef DAQ_COOLUTILS_COOLREADABLEVECTOR_H
#define DAQ_COOLUTILS_COOLREADABLEVECTOR_H

#include <CoolUtils/CoolReadable.h>

#include <CoolKernel/pointers.h>

#include <cstddef>


namespace daq
{
  namespace coolutils
  {
    class CoolReadableVector
    {
    public:
      // Constructor

      // Destructor
      virtual ~CoolReadableVector();
      
      // Accessor
      virtual cool::IObjectVectorPtr objects() const { return m_objectVectorPtr; }

      // Methods
      // Return an entry in the vector as individual CoolReadable object (that can be used for comparisons)
      virtual daq::coolutils::CoolReadable object( size_t i ) const;
      
    private:
      friend class CoolReaderEngine;

      // Methods
      virtual void set( cool::IObjectVectorPtr objectVectorPtr ) { m_objectVectorPtr = objectVectorPtr; }

      // Data members
      cool::IObjectVectorPtr m_objectVectorPtr;
    };
  }
}

#endif // DAQ_COOLUTILS_COOLREADABLEVECTOR_H
