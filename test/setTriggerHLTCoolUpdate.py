#!/bin/env python
# setTriggerHLTCoolUpdate.py - tool for storing FolderIndex-FolderName mapping in /TRIGGER/HLT/COOLUPDATE
# FolderIndex is stored in the COOL channelId
# author: Rainer Bartoldus <bartoldu@slac.stanford.edu>

import os,sys
from PyCool import cool
from CoolConvUtilities import AtlCoolLib

class setCoolUpdate(AtlCoolLib.coolTool):
    def setup(self,args):
        # set values of non-optional parameters
        self.tag        =str(args[0])
        self.FolderIndex=int(args[1])
        self.FolderName =str(args[2])
        
    def usage(self):
        """ Define the additional syntax for options """
        self._usage1()
        print 'COOL_tag FolderName'
        self._usage2()
        
    def execute(self):
        # do update - setup folder specification and create if needed
        spec=cool.RecordSpecification()
        spec.extend("FolderName" ,cool.StorageType.String255)
        folder='/TRIGGER/HLT/COOLUPDATE'
        print ">== Store object in folder",folder
        cfolder=AtlCoolLib.ensureFolder(self.db,folder,spec,AtlCoolLib.athenaDesc(self.runLumi,'AthenaAttributeList'),cool.FolderVersioning.MULTI_VERSION)
        if (cfolder is None): sys.exit(1)
        # now write data
        payload=cool.Record(spec)
        payload['FolderName']=self.FolderName
        print '>== Store object with IOV [',self.since,',',self.until,'] and tag',self.tag,'FolderIndex (channel)',self.FolderIndex
        print '>== FolderIndex: ',self.FolderIndex,'  FolderName: ',self.FolderName
        try:
            print '>== Lock status: ',cfolder.tagLockStatus( self.tag )
            locked = cfolder.tagLockStatus( self.tag )
            if ( locked ):
                cfolder.setTagLockStatus( self.tag, cool.HvsTagLock.UNLOCKED )
            cfolder.storeObject(self.since,self.until,payload,self.FolderIndex,self.tag)
            print ">== Storing COOL object succeeded"
            if ( locked ):
                cfolder.setTagLockStatus( self.tag, cool.HvsTagLock.LOCKED )
        except Exception,e:
            print e
            print '>== Storing COOL object FAILED'
            sys.exit(1)

mytool=setCoolUpdate('setTriggerHltCoolUpdate.py',False,4,4,[])
