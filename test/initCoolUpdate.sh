#/bin/bash

export CORAL_DBLOOKUP_PATH=/atlas/db/.adm/coral
export CORAL_DBAUTH_PATH=/atlas/db/.adm/coral

#DBCONNECTION="sqlite://;schema=myCoolUpdate.db;dbname=CONDBR2"
#DBCONNECTION="COOLONL_TRIGGER/CONDBR2"
DBCONNECTION='oracle://ATONR_COOL;schema=ATLAS_COOLONL_TRIGGER;dbname=CONDBR2;user=ATLAS_COOLONL_TRIGGER_W;password=WCOOLONL4TRIGGER12'

tag='TriggerHLTCoolUpdate-RUN2-UPD1-001-01'

show()
{
    { echo usetag $tag; echo more /TRIGGER/HLT/COOLUPDATE; } | AtlCoolConsole.py $DBCONNECTION
}

store()
{
    ./setTriggerHLTCoolUpdate.py --debug $DBCONNECTION $tag 0 '/Indet/Onl/Beampos'
    ./setTriggerHLTCoolUpdate.py --debug $DBCONNECTION $tag 1 '/TRIGGER/LUMI/HLTPrefLumi'
}

show
store
show
