#!/usr/bin/env tdaq_python

from __future__ import annotations

import unittest

import cool_utils


class TestCoolUtils(unittest.TestCase):
    """Tests for CoolUtils Python wrappers."""

    def test_folder_index(self) -> None:
        """Check that FolderIndex enum has expected values."""

        self.assertEqual(cool_utils.FolderIndex.BEAMSPOT, 0)
        self.assertEqual(cool_utils.FolderIndex.LUMINOSITY, 1)
        self.assertEqual(cool_utils.FolderIndex.HLTPRESCALE, 2)
        # If the list is expanded, this will fail and tell us that wrapper
        # needs to be updated.
        self.assertEqual(cool_utils.FolderIndex.FolderIndexMax, 2)


if __name__ == "__main__":
    unittest.main()
