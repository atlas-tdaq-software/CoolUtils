#
# import everything from lib_cool_utils_py
#
import sys

if sys.platform.startswith("linux"):
    # On Linux with g++ one needs RTLD_GLOBAL for dlopen which Python does
    # not set by default.
    import os
    flags = sys.getdlopenflags()
    sys.setdlopenflags(flags | os.RTLD_GLOBAL)
    from libpycool_utils import *  # noqa: F401, F403
    sys.setdlopenflags(flags)
    del flags
    del os
else:
    from libpycool_utils import *  # noqa: F401, F403

del sys
