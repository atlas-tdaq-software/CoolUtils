
class CTPUpdateConditions:

    def __init__(self, partition: str, controllerName: str ="CtpController"): ...
    def update(self, folderIndex: int, nextLBN: int = 0) -> None: ...


class FolderIndex:

    BEAMSPOT: int
    LUMINOSITY: int
    HLTPRESCALE: int
    FolderIndexMax: int
