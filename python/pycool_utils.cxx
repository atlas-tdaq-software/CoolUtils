#include <string>
#include <boost/python.hpp>

#include "ers/ers.h"
#include "CoolUtils/CTPConditionsUpdate.h"
#include "TriggerCommander/TriggerCommander.h"

using namespace boost::python;
namespace boopy = boost::python;

namespace {

ERS_DECLARE_ISSUE(
    cool_utils,
    InvalidPartition,
    "Partition is not valid: " << name,
    ((std::string) name)
)

class CTPUpdateConditions {
public:
    CTPUpdateConditions(std::string const& partition, std::string const& controllerName = "CtpController")
        : m_partition(partition), m_controllerName(controllerName)
    {
        if (not m_partition.isValid()) {
            throw ::cool_utils::InvalidPartition(ERS_HERE, partition);
        }
    }

    void update(int folderIndex, int nextLBN = 0) {
        daq::trigger::TriggerCommander commander(m_partition, m_controllerName);
        commander.setConditionsUpdate(folderIndex, nextLBN);
    }

private:
    IPCPartition m_partition;
    std::string const m_controllerName;
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(update_overloads, update, 1, 2)

BOOST_PYTHON_MODULE(libpycool_utils)
{
    // We want to use exception translator for ers::Issue installed by ers
    // module, need to import it.
    import("ers");

    class_<CTPUpdateConditions>(
        "CTPUpdateConditions",
        init<std::string, optional<std::string>>(
            (arg("partition"), arg("controllerName") = "CtpController")
        )
    )
    .def(
        "update",
        &CTPUpdateConditions::update,
        ::update_overloads(
            (arg("folderIndex"), arg("nextLBN") = 0)
        )
    );

    enum_<daq::coolutils::FolderIndex>("FolderIndex")
        .value("BEAMSPOT", daq::coolutils::BEAMSPOT)
        .value("LUMINOSITY", daq::coolutils::LUMINOSITY)
        .value("HLTPRESCALE", daq::coolutils::HLTPRESCALE)
        .value("FolderIndexMax", daq::coolutils::FolderIndexMax)
    ;
}

}
